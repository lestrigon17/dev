class Config {
    // Размерность поля
    public fieldSizeX: number = 10;  // Ось: Х
    public fieldSizeY: number = 11;  // Ось: У

    // Размер канвы
    public canvasSizeX: number = 1200; // Ось: Х
    public canvasSizeY: number = 690; // Ось: У

    // Размер тайлов и клеток
    public tileSize: number = 30;  // Размер спрайта тайла
    public cellSize: number = 62;  // Размер ячейки сетки

    // Скорость tile'ov
    public tileDropSpeed:    number = 0.200; // с
    public tileDestroySpeed: number = 0.300; // с
    public tileDissolveSpeed:number = 0.100; // с
    public doubleClickTimeout:number = 500; // мс
}

export var config: Config = new Config();