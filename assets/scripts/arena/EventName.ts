export var EventName = {
    // Game
    GameStart: "gameStart",
    GamePause: "gamePause",
    GameContinue: "gameContinue",
    GameLose: "gameLose",
    GameWin: "gameWin",

    // Menus
    MenuFail: "menuFail",
    MenuNewGame: "menuNewGame",
    MenuWinner: "menuWinner"
}