/**
 * Imports modules
 */

import { config } from "./config";
import { EventName } from "./EventName";
import { ItemTile } from "./items/ItemTile";
import { Coords, CoordAxis } from "./functions/position";
import { ItemGift } from "./items/ItemGift";

// Enums
import { stateCell, 
        stateTile, 
        stateTileType, 
        getRandomTileType, 
        stateGiftType, 
        stateTileSprite } from "./stateEnums";

/**
 * enum export
 */
export enum SceneStatus {
    spawning,
    isMoving,
    isSwiping,
    isDropping,
    exploding,
    active,
    menu
}

/**
 * Class Constructor
 */
const {ccclass, property} = cc._decorator;

@ccclass
export class ArenaController extends cc.Component {
    /**********************************
     **** @property
     **********************************/
    // # SPRITE NODE
    @property   (cc.Sprite) spriteChooser: cc.Sprite = null; // Обводка вокруг выбранного тайла
    @property   (cc.Sprite) spriteRequire: cc.Sprite = null; // Спрайт цель внутри задания

    // # SpriteFrames
    @property   (cc.SpriteFrame) spriteFrameRed:    cc.SpriteFrame = null; // Тайл: Красная текстура
    @property   (cc.SpriteFrame) spriteFrameOrange: cc.SpriteFrame = null; // Тайл: Оранжевая текстура
    @property   (cc.SpriteFrame) spriteFrameYellow: cc.SpriteFrame = null; // Тайл: Желтая текстура
    @property   (cc.SpriteFrame) spriteFrameGreen:  cc.SpriteFrame = null; // Тайл: Зеленая текстура
    @property   (cc.SpriteFrame) spriteFrameBlue:   cc.SpriteFrame = null; // Тайл: Голубая текстура
    @property   (cc.SpriteFrame) spriteFramePurple: cc.SpriteFrame = null; // Тайл: Фиолетовая текстура

    // # LABEL
    @property   (cc.Label)  labelMoves:   cc.Label = null; // Counter шагов
    @property   (cc.Label)  labelRequire: cc.Label = null; // Counter цели

    // # Buttons
    @property   (cc.Node) buttonBooster: cc.Node = null; // Кнопка : Бустер

    // # json configuration
    @property   (cc.JsonAsset) jsonLevelData: cc.JsonAsset = null; // Информация о поле

    // # prefabs
    @property   (cc.Prefab) itemGiftPrefab: cc.Prefab = null; // молния
    @property   (cc.Prefab) itemTilePrefab: cc.Prefab = null; // Тайл
    @property   (cc.Prefab) cellItemHole:   cc.Prefab = null; // Дырка на поле
    @property   (cc.Prefab) cellItemBlock:  cc.Prefab = null; // Блок для падения тайла

    // # Tile parent
    @property   (cc.Node) arenaTiles: cc.Node = null; // Родительский элемент для тайлов
    @property   (cc.Node) arenaHoles: cc.Node = null; // Родительский элемент для дырок
    @property   (cc.Node) arenaBlocks: cc.Node = null; // Родительский элемент для Блока

    /**********************************
     **** @private
     **********************************/

    // # LOCAL VALUES
    // Кол-Во шагов
    private _moves:    number = 0;   // Кол-во возможных шагов
    private _movesNow: number = 0;   // Сколько сделали шагов

    // ЦЕЛЬ ИГРЫ
    private _requireType:      stateTileType = null;// Требуемый тип тайлов
    private _requirePicked:    number        = 0;   // Кол-во необходимо собрать
    private _requirePickedNow: number        = 0;   // Сколько уже собрали

    // БУСТЕР
    private _boosterIsActive: boolean = false; // Активирован ли бустер
    private _boosterIsUsed:   boolean = false; // использован ли бустер

    // Работа с тачем
    private _touchTilePos:  cc.Vec2  = new cc.Vec2(0, 0); // Предыдущая точка которую подобрал тач
    private _touchTileNode: ItemTile = null; // Предыдущая точка которую подобрал тач
    private _touchLastClick:number   = 0;

    // Массив точек
    private _subMeta: [stateTileType[]] = [[]]; // Виртуальная мета поля
    private _meta: [{status: stateCell, node: any}[]] = [[]]; // Видимая мета поля
    private _metaSpawned: boolean = false; // Проверка на то, существует ли мета

    // Состояние сцены
    private _sceneStatus: SceneStatus = SceneStatus.spawning;

    // таблица спавна предыдущих точек при спавне новых
    private _spawnerData: stateTileType[] = []; // Таблица с предыдущие выпавшими тайлами

    // Отступы на поле
    private _arenaPaddingX: number = 0;
    private _arenaPaddingY: number = 0;

    
    /**********************************
     **** @functions
     **********************************/
    // События
    onLoad(): void {
        // Скрытие chooser
        this.spriteChooser.node.active = false;

        // Глобальные события
        cc.systemEvent.on(EventName.GameStart, this.gameStart, this); // Старт игры
        cc.systemEvent.on(EventName.GameContinue, this.gameContinue, this); // Продолжение игры

        // Работа с TOUCH касаниями
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);

        // Кнопки на игровом поле
        this.buttonBooster.on(cc.Node.EventType.TOUCH_START, this.fButtonBooser, this);
    }

    onDestroy(): void {
        cc.systemEvent.off(EventName.GameStart, this.gameStart, this);
        cc.systemEvent.off(EventName.GameContinue, this.gameContinue, this);

        this.buttonBooster.off(cc.Node.EventType.TOUCH_START, this.fButtonBooser, this);

        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    /*******************************************
     **** Перебор всего поля
     *******************************************/
    getEveryCell(callback: Function, fromEnd: boolean = false, byColumn: boolean = false): void {

        const columnCount = config.fieldSizeX;
        const rowCount = config.fieldSizeY;

        if (fromEnd) {
            if (byColumn) {
                for (let column = 0; column < columnCount; column++) {
                    for (let row = rowCount-1; row >= 0; row--) {
                        callback(column, row);
                    }
                }
                return;
            } 

            for (let row = rowCount-1; row >= 0; row--) {
                for (let column = 0; column < columnCount; column++) {
                    callback(column, row);
                }
            }

            return;
        }

        if (byColumn) {
            for (let column = 0; column < columnCount; column++) {
                for (let row = 0; row < rowCount; row++) {
                    callback(column, row);
                }
            }

            return;
        }

        for (let row = 0; row < rowCount; row++) {
            for (let column = 0; column < columnCount; column++) {
                callback(column, row);
            }
        }
        
    }

    /*******************************************
     **** Работа с TOUCH событиями
     *******************************************/
    onTouchStart(event: cc.Event.EventTouch): void {

        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        const coordNow  = this.getCoordFromPosition(event.getLocation());
        const coordLast = this.getCoordFromPosition(this._touchTilePos);

        let cursorCell  = this.getCell(coordNow);

        let isDoubleClicked: boolean = false;

        if (this._touchLastClick + config.doubleClickTimeout > Date.now()) {
            isDoubleClicked = true;
        }

        this._touchLastClick = Date.now();

        if (this._boosterIsActive && isDoubleClicked) {
            if (cursorCell.node.isTile) {
                this.tileRemove(coordNow);
            }
            
            if (cursorCell.node.isGift) {
                this.giftExplode(coordNow);
            }

            this.gameAddMoves();
            this.clearTouchTile();
            return;
        }

        if (this._touchTileNode == null) {
            if (cursorCell.status == stateCell.active) {
                this._touchTilePos = event.getLocation();
                this._touchTileNode = cursorCell.node;
            }

            return;
        }
        
        if (cursorCell.status != stateCell.active || this._touchTileNode == cursorCell.node) {
            if (isDoubleClicked && cursorCell.node.isGift) {
                this.giftExplode(coordNow);
            }

            this._touchTileNode = null;
            this._touchTilePos = cc.v2(0, 0);
            return;
        }

        const canSwipe = this.tileCanSwipe(coordLast, coordNow);

        if (canSwipe == false) {
            this._touchTileNode = cursorCell.node;
            this._touchTilePos = event.getLocation();
            return;
        }

        const response = this.metaMoveTile(coordLast, coordNow);

        if (response) {
            this.gameAddMoves();
        } else {
            this.tileFakeSwipe(coordLast, coordNow);
        }

        this.clearTouchTile()
    }

    onTouchMove(event: cc.Event.EventTouch): void {

        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }

        if (this._touchTileNode == null) {
            return;
        }

        const xDelta = Math.abs(this._touchTilePos.x - event.getLocationX());
        const yDelta = Math.abs(this._touchTilePos.y - event.getLocationY());

        const maxDelta: number = config.cellSize/2;

        const coordStart = this.getCoordFromPosition(this._touchTilePos);
        const coordEnd   = this.getCoordFromPosition(event.getLocation());
        
        if (xDelta > maxDelta && yDelta > maxDelta) {
            this.clearTouchTile();
            return;
        }

        if (coordStart != coordEnd) {
            const canSwipe = this.tileCanSwipe(coordStart, coordEnd);

            if (canSwipe == false) {
                return;
            }
            
            const response = this.metaMoveTile(coordStart, coordEnd);

            if (response) {
                this.gameAddMoves();
            } else {
                this.tileFakeSwipe(coordStart, coordEnd);
            }

            this.clearTouchTile();
        }
    }

    clearTouchTile(): any {
        this._touchTileNode = null;
        this._touchTilePos = cc.v2(0, 0);
    }

    onTouchEnd(event: cc.Event.EventTouch): void {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }

        let lastClickWithTimeOut = this._touchLastClick + config.doubleClickTimeout * 2;
        if (lastClickWithTimeOut > Date.now()) {
            return;
        }

        this.clearTouchTile();
    }

    /*******************************************
     **** Работа с игровой механикой
     *******************************************/
    /**
     * Генерация ячеек на поле и суб-поле
     * Заполнение таблицы пустотой
     */
    genMeta():void {
        if (this._metaSpawned) {
            return;
        }

        this.getEveryCell((column, row) => {
            if (!Array.isArray(this._meta[row])) {
                this._meta[row] = [];
            }
            if (!Array.isArray(this._subMeta[row])) {
                this._subMeta[row] = [];
            }
            
            this._meta[row][column] = {
                status: stateCell.blocked,
                node: null
            };
            this._subMeta[row][column] = null;
        });

        this._metaSpawned = true;
    }

    /**
     * Очищение поля
     */
    gameClear(): void {
        if (!this._metaSpawned) return;

        this.getEveryCell((column, row) => {
            let cell = this.getCell(new Coords(column, row));

            if (cell.status != stateCell.active) {
                return;
            }

            cell.node.destroy();
            cell.status = stateCell.hole;
        })
    }

    /**
     * Иниаициализация поля
     * Установка и генерация тайлов
     */
    gameBuildArena(): void {
        const fieldData = this.jsonLevelData.json;
        
        let oldType:  stateTileType = null;
        let downType: stateTileType = null;

        this.getEveryCell((column, row) => {
            let coordCell = new Coords(column, row);
            let tileType = fieldData.field_dots[row][column];
            
            let cell = this.getCell(coordCell);
            let metaCell = this._subMeta[row][column];
            
            if (tileType == -1) { 
                this.cellCreateBGHole(coordCell);
            } else {
                this.cellCreateBGBlock(coordCell);
            }

            switch (tileType) {
                case -1: 
                    metaCell = null;
                    cell.status = stateCell.banned;

                    break;

                case 0:
                    metaCell = null;
                    cell.status = stateCell.hole;

                    break;

                case 99:
                    if (row > 0) {
                        let downCell = this.getCell(new Coords(column, row-1));
                        if (downCell.status == stateCell.active) {
                            downType = downCell.node.getType();
                        }
                    }
                    let randomType = this.tileGetRandomType([oldType, downType]);
                    this.tileCreate(coordCell, randomType, true);
                    oldType = randomType;

                    break;

                case 100:
                        this.giftCreate(stateGiftType.horizontal, coordCell);
                    break;
                    
                case 101:
                        this.giftCreate(stateGiftType.vertical, coordCell);
                    break;

                default:
                    let itemType = stateTileType[stateTileType[tileType]];

                    this.tileCreate(coordCell, itemType, true);
                    oldType = itemType;

                    break;
            }
        });
    }

    /**
     * Начало новой игры
     */
    gameStart(): void {
        this.setSceneStatus(SceneStatus.spawning);
        this.gameClear();

        this.genMeta(); // генерация меты поля
        this.gameBuildArena(); // генерация тайлов на поле
        /**
         * Установка целей
         */
        let jsonData = this.jsonLevelData.json;
        this._moves = jsonData.level_moves;
        this._requirePicked = jsonData.level_require;
        this._requireType = stateTileType[stateTileType[jsonData.level_require_type]];

        this._movesNow = 0;
        this._requirePickedNow = 0;

        this._boosterIsActive = false;
        this._boosterIsUsed = false;
        this.setSceneStatus(SceneStatus.active);
    }
    /**
     * Продолжение старой игры
     */
    gameContinue(): void {
        this._moves += 5;
        this.setSceneStatus(SceneStatus.active);
    }
    /*******************************************
     **** Работа с ячейками
     *******************************************/

    /**
     * Блокировка ячейки для каких-либо манипуляций
     */
    banCell(coords: Coords): void {
        this._meta[coords.row][coords.column] = {
            status: stateCell.banned,
            node: null
        }
    }
    /**
     * Получение ячейки с содержимым
     */
    getCell(coords: Coords): {status: stateCell, node: any} {
        if (coords.column < 0 ||
            coords.row < 0 ||
            coords.column >= config.fieldSizeX ||
            coords.row >= config.fieldSizeY) {

            return {status: stateCell.banned, node: null};
        }
            
        return this._meta[coords.row][coords.column];
    }
    /**
     * Добавление в ячейку тайла
     * @param node - Нод от тайла
     */
    addCellItem(node: (ItemTile|ItemGift), coords: Coords): void {
        
        if (node instanceof ItemTile) {
            this._subMeta[coords.row][coords.column] = node.getType();
        } else {
            this._subMeta[coords.row][coords.column] = null;
        }

        this._meta[coords.row][coords.column] = {
            status: stateCell.active,
            node: node
        };
    }
    /**
     * Установка статуса для конкретной ячейки на пле
     * @param status - Требуемый статус
     * @param backupTimeout - На какое время выставляется статус. 0 - без возвратного таймера
     */
    setCellStatus(coords: Coords, status: stateCell, backupTimeout: number = 0): void {
        const cell = this.getCell(coords);
        const oldStatus = cell.status;

        cell.status = status;

        if (backupTimeout == 0) {
            return;
        }

        this.scheduleOnce(() => {
            cell.status = oldStatus;
        }, backupTimeout);
    }

    // # Работа с виртуальным полем
    cellCreateBGHole(coordCreate: Coords): void {
        return;
        /*// Создание нода
        let node: cc.Node = cc.instantiate(this.cellItemHole); 
        // Добавление на сцену
        this.arenaBlocks.addChild(node)
        // Установка позиции
        let pos: cc.Vec2 = this.getPositionFromCoords(coordCreate);
        node.setPosition(pos);

        return node;*/
    }

    cellCreateBGBlock(coordCreate: Coords): cc.Node {
        let node = cc.instantiate(this.cellItemBlock); 
        this.arenaHoles.addChild(node);

        let pos = this.getPositionFromCoords(coordCreate);
        node.setPosition(pos);

        return node;
    }
    /**
     * Проверка, можем ли мы свайпнуть точки между собой и создаст ли это комбинацию
     */
    metaMoveTile(coordStart: Coords, coordEnd: Coords): boolean {
        // CElLS
        let data = [
            {
                cell: this.getCell(coordStart),
                meta: this._subMeta[coordStart.row][coordStart.column],
                coordStart: coordStart,
                coordEnd: coordEnd
            },
            {
                cell: this.getCell(coordEnd),
                meta: this._subMeta[coordEnd.row][coordEnd.column],
                coordStart: coordEnd,
                coordEnd: coordStart
            }
        ]

        let haveGift = false;

        for (let value of data) {
            if (value.cell.node.isGift) {
                haveGift = true;
            }

            this._subMeta[value.coordEnd.row][value.coordEnd.column] = value.meta;
        }

        // FIND COMBINATIONS
        let haveSuccessCombination = false;
        
        let combinations = [
            {
                response: null,
                coords: coordStart
            },
            {
                response: null,
                coords: coordEnd
            },
        ]

        for (let combination of combinations) {
            combination.response = this.metaFindCombinations(combination.coords);

            if (combination.response.found == true) {
                haveSuccessCombination = true;
            }
        }

        for (let value of data) {
            this._subMeta[value.coordStart.row][value.coordStart.column] = value.meta;
        }

        if (!haveSuccessCombination && !haveGift) {
            return false;
        }

        // ACTIONS
        this.setSceneStatus(SceneStatus.isSwiping, config.tileDestroySpeed);
        this.tileSwipe(coordStart, coordEnd);

        // FOR CUSTOM TILES
        for (let value of data) {
            if (!value.cell.node.isGift) {
                continue;
            }

            this.scheduleOnce(() => {
                this.giftExplode(value.coordEnd);
            }, config.tileDropSpeed);
        }

        // DESTROY COMBINATION
        for (let combination of combinations) {
            let response = combination.response;
            if (response.found == false) {
                continue;
            }

            this.scheduleOnce(() => {
                for (let coord of response.combo) {
                    this.tileRemove(coord);
                }

                if (response.count.x == 4) {
                    this.giftCreate(stateGiftType.horizontal, combination.coords);
                    return;
                }
                
                if (response.count.y == 4) {
                    this.giftCreate(stateGiftType.vertical, combination.coords);
                }
            }, config.tileDestroySpeed);
        }

        return true;
    }


    /**
     * Поиск комбинаций у конкретной точки
     * @param type - Тип точки
     */
    metaFindCombinations(coords: Coords): {} {
        let haveCombinations: boolean = false;

        let columnCount: number  = 0;
        let rowCount: number     = 0;

        let dotsComboColumn: number[] = [];
        let dotsComboRow: number[]    = [];

        let type = this._subMeta[coords.row][coords.column];

        if (type == null || type > 99) {
            return {
                found: false,
                combo: [],
                count: new cc.Vec2(0, 0)
            };
        }

        for (let direction = -1; direction <= 1; direction += 2) {
            for (let column = coords.column+direction; direction < 0 && column >= 0 || column < config.fieldSizeX; column += 1*direction) {
                if (column < 0 || column > config.fieldSizeX) {
                    break;
                }

                let typeSecond = this._subMeta[coords.row][column];

                if (typeSecond != type || typeSecond == null) {
                    break;
                }

                dotsComboColumn.push(column);
                columnCount ++;
            }
        }

        for (let direction = -1; direction <= 1; direction += 2) {
            for (let row = coords.row+direction; direction < 0 && row >= 0 || row < config.fieldSizeY; row += 1*direction) {
                if (row < 0 || row > config.fieldSizeY) {
                    break;
                }

                let typeSecond = this._subMeta[row][coords.column];

                if (typeSecond != type || typeSecond == null) {
                    break;
                }

                dotsComboRow.push(row);
                rowCount ++;
            }
        }

        let tileComboStore: Coords[] = [];

        if (dotsComboColumn.length >= 2) {
            for (let column of dotsComboColumn){
                tileComboStore.push(new Coords(column, coords.row));
            }

            haveCombinations = true;
            columnCount += 1;
        }

        if (dotsComboRow.length >= 2) {
            for (let row of dotsComboRow) {
                tileComboStore.push(new Coords(coords.column, row));
            }

            haveCombinations = true;
            rowCount += 1;
        }

        if (haveCombinations) {
            tileComboStore.push(coords);
        }

        return {
            found: haveCombinations,
            combo: tileComboStore,
            count: new cc.Vec2(columnCount, rowCount)
        };
    }

    /**
     * Поиск всех возможных комбинаций на поле
     */
    metaFindAllCombinations(): void {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }

        let tempComboItems: Coords[] = [];
        let comboLastType: stateTileType = null;
        let comboItems: [[Coords[], stateGiftType]] = [[[], null]];
        let comboCount: number = 0;

        // rows
        this.getEveryCell((column, row) => {
            let coordCell = new Coords(column, row);
            let cell = this.getCell(coordCell);
            let cellItem = cell.node;
            let cellStatus = cell.status;

            if (cellStatus != stateCell.active || !cellItem.isTile) {
                if (comboCount >= 3) comboItems.push([tempComboItems, stateGiftType.horizontal]);
                tempComboItems = [];
                comboCount = 0;
                comboLastType = null;
                return;
            }

            if (comboLastType == null || comboLastType == cellItem.getType()) {
                comboLastType = cellItem.getType();
                comboCount += 1;
                tempComboItems.push(coordCell);
                return;
            }

            if (comboCount >= 3) {
                comboItems.push([tempComboItems, stateGiftType.horizontal]);
            }
            tempComboItems = [];
            tempComboItems.push(coordCell);
            comboCount = 1;
            comboLastType = cellItem.getType();
            return;
        });

        // column
        this.getEveryCell((column, row) => {
                let coordCell = new Coords(column, row);
                let cell = this.getCell(coordCell);
                let cellItem = cell.node;
                let cellStatus = cell.status;

                if (cellStatus != stateCell.active || !cellItem.isTile) {
                    if (comboCount >= 3) {
                        comboItems.push([tempComboItems, stateGiftType.vertical]);
                    }
                    tempComboItems = [];
                    comboCount = 0;
                    comboLastType = null;
                    return;
                }

                if (comboLastType == null || comboLastType == cellItem.getType()) {
                    comboLastType = cellItem.getType();
                    comboCount += 1;
                    tempComboItems.push(coordCell);
                    return;
                }

                if (comboCount >= 3) {
                    comboItems.push([tempComboItems, stateGiftType.vertical]);
                }
                
                tempComboItems = [];
                tempComboItems.push(coordCell);
                comboCount = 1;
                comboLastType = cellItem.getType();
                return;
        }, false, true)

        if (comboItems.length >= 2) {
            for (let combItem of comboItems) {
                let coordStart = combItem[0][0]
                    
                for (let coordCell of combItem[0]) {
                    this.tileRemove(coordCell);
                }

                if (combItem[0].length == 4) {
                    this.giftCreate(combItem[1], coordStart);
                }
            }

            this.setSceneStatus(SceneStatus.isSwiping, config.tileDestroySpeed);
        }
    }

    /*******************************************
     **** Работа с гифтами
     *******************************************/
    /**
     * Создание молнии
     * @param type - Тип вертикальный или горизонтальный
     * @param coordCreate - Позиция
     */
    giftCreate(type: stateGiftType, coordCreate: Coords): ItemGift {
        let node: cc.Node = cc.instantiate(this.itemGiftPrefab); 
        this.arenaTiles.addChild(node)

        let nodeGift = node.getComponent(ItemGift);
        nodeGift.setType(type);

        let pos = this.getPositionFromCoords(coordCreate);
        nodeGift.setPos(pos);

        this._subMeta[coordCreate.row][coordCreate.column] = null;

        this.addCellItem(nodeGift, coordCreate);

        return nodeGift;
    }

    giftExplode(coordExplosion: Coords): any {
        let cell = this.getCell(coordExplosion);

        if (cell.node.IsValid && !cell.node.isGift) {
            return;
        }

        this.setSceneStatus(SceneStatus.exploding);

        let giftType: stateGiftType = cell.node.getType();

        if (giftType == stateGiftType.horizontal) {
            this.giftCheck(CoordAxis.row, coordExplosion);
        } else {
            this.giftCheck(CoordAxis.column, coordExplosion);
        }
        
        this.tileRemove(coordExplosion);
    }

    giftCheck(align: string, coords: Coords): void {
        let dir = align == CoordAxis.row && config.fieldSizeX || config.fieldSizeY;

        let coordSec = (align == CoordAxis.row) && CoordAxis.column || CoordAxis.row;

        for (let direction = -1; direction <= 1; direction += 2) {
            for (let coord = coords[coordSec]+direction; direction < 0 && coord >= 0 || coord < dir; coord += direction) {
                if (coord < 0 || coord > dir) {
                    break;
                }
                
                let coordCell = null;

                if (align == CoordAxis.row) {
                    coordCell = new Coords(coord, coords.row);
                }

                if (align == CoordAxis.column) {
                    coordCell = new Coords(coords.column, coord);
                }

                let cellNext = this.getCell(coordCell);

                if (cellNext.status != stateCell.active) {
                    continue;
                }
                cellNext.status = stateCell.destroy;

                let scheduleTimer = config.tileDissolveSpeed * Math.abs(coords[coordSec] - coord);

                this.scheduleOnce(() => {
                    if (cellNext.status != stateCell.destroy) {
                        return;
                    }

                    if (cellNext.node.isValid && cellNext.node.isGift) {
                        this.giftExplode(coordCell);
                        return;
                    }

                    this.tileRemove(coordCell);
                }, scheduleTimer);
            }
        }

    }

    /*******************************************
     **** Работа с тайлами
     *******************************************/

    /**
     * Создание тайла
     * @param force - Требуется ли проигрывать анимацию появления. true - если да
     */
    tileCreate(coordCreate: Coords, type: stateTileType, force: boolean = false): ItemTile {
        let node = cc.instantiate(this.itemTilePrefab); 
        this.arenaTiles.addChild(node)

        let nodeTile = node.getComponent(ItemTile);
        nodeTile.spawn(force);
        
        let pos = this.getPositionFromCoords(coordCreate);
        nodeTile.setPos(pos);

        nodeTile.setType(type); 
        this._subMeta[coordCreate.row][coordCreate.column] = type;

        this.addCellItem(nodeTile, coordCreate);

        return nodeTile;
    }

    /**
     * Получение рандомного типа для ячейки
     * @param excludeTypes - Список типов которые запрещены к рассмотрению
     */
    tileGetRandomType(excludeTypes: stateTileType[] = []): stateTileType {
        let availableTypes: stateTileType[] = [];

        for (var type of getRandomTileType()) {
            if (excludeTypes.indexOf(type) >= 0) {
                continue;
            }
            availableTypes.push(type);
        }

        let randomID = randomInteger(0, availableTypes.length-1);

        return availableTypes[randomID];
    }

    /*******************************************
     **** Работа с движением ячеек
     *******************************************/

    /**
     * Вычесление расстояния между двумя ячейками (горизонталь|вертикаль)
     */
    tileCanSwipe(coordStart: Coords, coordEnd: Coords): boolean {
        let column = Math.abs(coordStart.column - coordEnd.column);
        let row = Math.abs(coordStart.row - coordEnd.row);

        if (column > 1 || row > 1) {
            return false;
        }

        let cell1 = this.getCell(coordStart);
        if (cell1.status != stateCell.active) {
            return false;
        }

        let cell2 = this.getCell(coordEnd);
        if (cell2.status != stateCell.active) {
            return false;
        }

        let diff = Math.abs( column - row );
        if (diff == 1) {
            return true;
        }

        return false;
    }

    /**
     * Фейковый свайп
     */
    tileFakeSwipe(coordStart: Coords, coordEnd: Coords): boolean {
        let data = [
            {
                cell: this.getCell(coordStart),
                posStart: this.getPositionFromCoords(coordStart),
                posEnd: this.getPositionFromCoords(coordEnd) 
            },
            {
                cell: this.getCell(coordEnd),
                posStart: this.getPositionFromCoords(coordEnd),
                posEnd: this.getPositionFromCoords(coordStart) 
            }
        ];

        for (let value of data) {
            if (value.cell.status == stateCell.active && value.cell.node.getStatus() == stateTile.active) {

                value.cell.status = stateCell.swipe;
                let timeoutFirst = value.cell.node.setPos(value.posEnd, true);

                this.scheduleOnce(() => {

                    let timeoutSecond = value.cell.node.setPos(value.posStart, true);

                    this.scheduleOnce(() => {
                        value.cell.status = stateCell.active;
                    }, timeoutSecond);

                }, timeoutFirst);

            }
        }

        return true;
    }
    /**
     * Движение с точки на точку
     */
    tileSwipe(coordStart: Coords, coordEnd: Coords): boolean {
        let data = [
            {
                cell: this.getCell(coordStart),
                coordsNew: coordEnd
            },
            {
                cell: this.getCell(coordEnd),
                coordsNew: coordStart
            }
        ];

        for (let value of data) {

            if (value.cell.status == stateCell.active && value.cell.node.getStatus() == stateTile.active) {
                let coordsNew = this.getPositionFromCoords(value.coordsNew);
                value.cell.node.setPos(coordsNew, true);
            }

            this._meta[value.coordsNew.row][value.coordsNew.column] = value.cell;
        }


        let metaCell1 = this._subMeta[coordStart.row][coordStart.column];
        let metaCell2 = this._subMeta[coordEnd.row][coordEnd.column];

        this._subMeta[coordStart.row][coordStart.column] = metaCell2;
        this._subMeta[coordEnd.row][coordEnd.column] = metaCell1;

        return true;
    }

    /**
     * Удаление тайла
     * @param x - Позиция точки
     * @param y - Позиция точки
     */
    tileRemove(coords: Coords): boolean {
        let cell = this._meta[coords.row][coords.column];

        if (cell.node.isTile) {
            this.gameAddRequire(cell.node.getType());
            cell.node.runAnimation("destroy");
        }

        if (cell.node.isGift) {
            cell.node.startRemove();
        }

        cell.status = stateCell.destroy;
        this._subMeta[coords.row][coords.column] = null;

        this.scheduleOnce(() => {
            if (cell.status != stateCell.destroy) {
                return;
            }

            if (cell.node.isValid) {
                cell.node.destroy();
            }

            cell.status = stateCell.hole;
            cell.node = null;
        }, config.tileDestroySpeed);


        return true;
    }

    /*******************************************
     **** Работа с сценой
     *******************************************/
    getSceneStatus():SceneStatus {
        return this._sceneStatus;
    }
    /**
     * @param backupTimeout - На какое время выставляется статус. 0 - без возвратного таймера
     */
    
    setSceneStatus(status: SceneStatus, backupTimeout: number = 0): void {
        let oldStatus = this._sceneStatus; 
        this._sceneStatus = status;

        if (backupTimeout == 0) {
            return;
        }

        this.scheduleOnce(() => {
            if (this._sceneStatus != status) {
                return;
            }
            this._sceneStatus = oldStatus;
        }, backupTimeout);
    }

    /**
     * Поиск дырок, в которые могут упасть тайлы
     */
    moveHolesVertical(): boolean {
        let sceneStatus = this.getSceneStatus();
        if (sceneStatus == SceneStatus.exploding) {
            return;
        }
        let haveHoles: boolean = false;

        let oldColumn: number = 0;
        let holeCounts: number = 0;

        this.getEveryCell((column, row) => {
            if (oldColumn != column) {
                oldColumn = column;
                holeCounts = 0;
            }

            const coordSwipeFrom = new Coords(column, row);
            const coordSwipeTo = new Coords(column, row+holeCounts);
            const cell = this.getCell(coordSwipeFrom);
            
            if (cell.status == stateCell.active && cell.node.getStatus() == stateTile.active && holeCounts > 0) {
                this.tileSwipe(coordSwipeFrom, coordSwipeTo);
                haveHoles = true;
                return;
            }

            if (cell.status == stateCell.hole) {
                holeCounts++; 
                return;
            }
            
            holeCounts = 0;

        }, true, true);
        
        if (haveHoles && sceneStatus == SceneStatus.active) {
            this.setSceneStatus(SceneStatus.isMoving);
        }

        if (!haveHoles && sceneStatus == SceneStatus.isMoving) {
            this.setSceneStatus(SceneStatus.active);
        }

        return haveHoles;
    }

    moveHolesDiagonal(): boolean {
        if (this.getSceneStatus() == SceneStatus.exploding) {
            return;
        };

        let haveHoles: boolean = false;
        let oldColumn: number = 0;
        let coordsHole: number[] = [];
        let coordsFinded: number[] = [];
        
        this.getEveryCell((column, row) => {
            if (oldColumn != column) {

                if (coordsHole.length > 0) {
                    this.diagonalCheck(oldColumn, coordsHole);
                    coordsHole = [];
                }

                oldColumn = column;
            }

            let cell = this.getCell(new Coords(column, row));

            if (cell.status == stateCell.banned || cell.status == stateCell.blocked) {
                if (coordsFinded.length > 0) {
                    coordsHole.push(coordsFinded[coordsFinded.length-1]);
                }
                coordsFinded = [];
                return;
            }

            if (cell.status != stateCell.hole && coordsFinded.length > 0) {
                coordsFinded = [];
                return;
            }

            if (cell.status != stateCell.hole) {
                return;
            }

            coordsFinded.push(row);
        }, true, true);

        if (coordsHole.length > 0) {
            this.diagonalCheck(oldColumn, coordsHole);
            coordsHole = [];
        }

        return haveHoles;
    }

    diagonalCheck(column, coordsHole): void {
        for (let coordRow of coordsHole) {
            let coordHole = new Coords(column, coordRow);

            let coordTopLeft = new Coords(column - 1, coordRow - 1);
            let coordTopRight = new Coords(column + 1, coordRow - 1);

            let cellLeft = this.getCell(coordTopLeft);
            let cellRight = this.getCell(coordTopRight);

            if (cellLeft.status == stateCell.banned && cellRight.status == stateCell.banned) {
                this.getCell(coordHole).status = stateCell.blocked;
            }

            if (cellLeft.status != stateCell.active && cellRight.status != stateCell.active) {
                continue;
            }

            let cellToDrop: number = 0;

            if (cellLeft.status == stateCell.active && cellRight.status == stateCell.active) {
                cellToDrop = randomInteger(0, 1);
            } else {
                if (cellLeft.status == stateCell.active) {
                    cellToDrop = 0
                } else {
                    cellToDrop = 1;
                }
            }

            if (cellToDrop == 1 && cellRight.node.getStatus() == stateTile.active) {
                this.tileSwipe(coordTopRight, coordHole);
            }

            if (cellToDrop == 0 && cellLeft.node.getStatus() == stateTile.active) {
                this.tileSwipe(coordTopLeft, coordHole);
            }
        }
    }

    /**
     * Поиск возможностей спавна тайла сверху поля
     */
    spawnTiles(): boolean {
        const sceneStatus = this.getSceneStatus();

        if (sceneStatus == SceneStatus.exploding) {
            return;
        }

        let isSpawned: boolean = false;

        const spawners = this.jsonLevelData.json.field_generation;
        
        for (let spawn of spawners) {
            const coords = new Coords(spawn.column, spawn.row);
            const cell = this.getCell(coords);
            
            if (cell.status == stateCell.hole) { 
                this.spawnTile(coords);
                isSpawned = true;
            }
        }
        
        if (isSpawned && sceneStatus == SceneStatus.active) {
            this.setSceneStatus(SceneStatus.isDropping);
        }

        if (!isSpawned && sceneStatus == SceneStatus.isDropping) {
            this.setSceneStatus(SceneStatus.active);
        }

        return isSpawned;
    }

    spawnTile(coords: Coords): void {
        let spawnID = coords.column+"_"+coords.row;
        let excludeType = this._spawnerData[spawnID];

        let type = this.tileGetRandomType([excludeType]);
        this._spawnerData[spawnID] = type;

        let node = this.tileCreate(coords, type);

        let pos = this.getPositionFromCoords(coords);
        
        node.setPos(new cc.Vec2(pos.x, pos.y+config.cellSize)); // Выставление на клетку выше чем спавн
        node.setPos(pos, true); // Движение клетки вниз до нужной точки
    }

    /**
     * Поиск не упавших нодов
     */
    checkTiles(): boolean {
        let haveInDropping: boolean = false;
        
        this.getEveryCell((column, row) => {
            if (haveInDropping) {
                return;
            }

            let cell = this.getCell(new Coords(column, row));

            if (cell.status == stateCell.destroy) {
                haveInDropping = true;
            }

            if (cell.status != stateCell.active) {
                return;
            }

            if (cell.node.getStatus() == stateTile.moving) {
                haveInDropping = true;
            }
        });

        if (this.getSceneStatus() == SceneStatus.exploding && !haveInDropping) {
            this.setSceneStatus(SceneStatus.active);
        }

        return haveInDropping;
    }

    /*******************************************
     **** Работа с координатами
     *******************************************/

    /**
     * Получение позиции точки из глобальный координат
     */
    getCoordFromPosition(pos: cc.Vec2): Coords {
        let column = Math.floor((pos.x - this._arenaPaddingX) / config.cellSize);
        if (column > config.fieldSizeX-1) {
            column = -1;
        }

        let row = Math.floor(Math.abs(config.canvasSizeY - pos.y - this._arenaPaddingY) / config.cellSize);
        if (row > config.fieldSizeY-1) {
            row = -1;
        }

        return new Coords(column, row);
    }

    /**
     * Получение глобальной позиции из локальных координат точки
     */
    getPositionFromCoords(coords: Coords): cc.Vec2 {
        let column = this._arenaPaddingX + config.cellSize * (coords.column) + config.cellSize/2;
        let row = Math.abs(config.canvasSizeY - this._arenaPaddingY - config.cellSize * (coords.row) - config.cellSize/2);

        return new cc.Vec2(column, row);
    }

    /*******************************************
     **** Добавление ходов и целей
     *******************************************/
    gameAddRequire(type: stateTileType): void {
        if (this._requireType == type) {
            this._requirePickedNow += 1;
        }
    }

    gameAddMoves(): void {
        if (!this._boosterIsActive) {
            this._movesNow += 1;
            return;
        }

        this._boosterIsActive = false;
        this._boosterIsUsed = true;

        this.buttonBooster.getComponent(cc.Animation).stop("boosterClick");
    }

    /*******************************************
     **** Отображение информации
     *******************************************/
    drawInfo(): void {
        let nowMoves = this._moves - this._movesNow;
        let nowRequire = this._requirePicked - this._requirePickedNow;

        this.labelMoves.string = nowMoves < 0 && "0" || nowMoves+"";
        this.labelRequire.string =  nowRequire < 0 && "0" || nowRequire+"";

        let spriteFrameRequire = this[stateTileSprite[this._requireType]];

        if (this.spriteRequire.spriteFrame != spriteFrameRequire) {
            this.spriteRequire.spriteFrame = spriteFrameRequire;
        }
    }

    /*******************************************
     **** Проверка целей
     *******************************************/
    checkRequires(): void {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }

        let nowMoves = (this._moves - this._movesNow);
        let nowRequire = (this._requirePicked - this._requirePickedNow);

        if (nowMoves <= 0 && nowRequire > 0) {
            cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName.MenuFail, true));
            this.setSceneStatus(SceneStatus.menu);
            return;
        }

        if (nowRequire <= 0) {
            this.gameClear();
            cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName.MenuWinner, true));
            this.setSceneStatus(SceneStatus.menu);
            return;
        }
    }

    /*******************************************
     **** Кнопки на поле
     *******************************************/
    /**
     * Кнопка бустера
     */
    fButtonBooser(event: cc.Event.EventTouch): void {
        if (this._boosterIsUsed || this._boosterIsActive) {
            return;
        }

        this._boosterIsActive = true;

        this.buttonBooster.getComponent(cc.Animation).play("boosterClick");
    }

    start(): void {
        // Вычесление отступов на поле
        this._arenaPaddingX = (config.canvasSizeX - (config.cellSize * config.fieldSizeX)) / 2;
        this._arenaPaddingY = (config.canvasSizeY - (config.cellSize * config.fieldSizeY)) / 2;
    }

    /*******************************************
     **** Update func per frame
     *******************************************/
    update(dt): void {
        const sceneState = this.getSceneStatus();
        if (sceneState == SceneStatus.menu || sceneState == SceneStatus.spawning) {
            return;
        }

        this.moveHolesVertical();
        this.moveHolesDiagonal();

        this.spawnTiles();

        let inMovingTiles = this.checkTiles();
        if (inMovingTiles == false) {
            this.metaFindAllCombinations();
        }

        this.drawInfo();
        this.checkRequires();

        if (this._touchTileNode != null && this._touchTileNode.isValid) {
            this.spriteChooser.node.active = true;
            this.spriteChooser.node.setPosition(this._touchTileNode.node.getPosition());
        } else {
            if (this.spriteChooser.node.active != false) {
                this.spriteChooser.node.active = false;
            }
        }
    }
}

/**
 * Рандомное целочисленное значение
 * @param min - от включительно
 * @param max - до включительно
 */
let randomInteger = function(min: number, max: number): number {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}