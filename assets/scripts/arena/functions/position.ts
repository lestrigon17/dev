export class Coords {
    public column: number = 0; // x
    public row: number = 0; // y

    constructor (column: number, row: number) {
        this.column = column;
        this.row = row;
    }
}

export var CoordAxis = {
    row: "row",
    column: "column"
}