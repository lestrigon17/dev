/**
 * Imports modules
 */
import { stateGiftType, stateTile } from "../stateEnums";
import { config } from "../Config";

/**
 * Class Constructor
 */
const {ccclass, property} = cc._decorator;

@ccclass
export class ItemGift extends cc.Component {
    // # Property
    @property   (cc.SpriteFrame) spriteFrameHorizontal: cc.SpriteFrame = null; // Текстура горизонтального подарка
    @property   (cc.SpriteFrame) spriteFrameVertical:   cc.SpriteFrame = null; // Текстура вертикального подарка

    // # Public
    public isGift: boolean = true;
    public isTile: boolean = false;

    // # Private
    private _type: stateGiftType = null;
    private _status: stateTile = stateTile.active;

    onLoad():void {

    }
    onDestroy():void {
        this.node.destroy();
    }
    IsGift():boolean {
        return true;
    }

    // # Position
    setPos(pos:cc.Vec2, force: boolean = false):number {
        if (force) {
            let abcX: number = Math.abs(this.node.position.x - pos.x);
            let abcY: number = Math.abs(this.node.position.y - pos.y);
            if (abcY > abcX) abcX = abcY;

            let time: number = Math.round(abcX / config.cellSize) * config.tileDropSpeed;
            this.setStatus(stateTile.moving, time);
            let animation: cc.Action = cc.moveTo(time, pos).easing(cc.easeCircleActionInOut());
            this.node.runAction(animation);

            this.scheduleOnce(() => {
                this.setStatus(stateTile.active);
            }, time);

            return time;
        }

        this.node.setPosition(pos);
        return 0;
    }

    // # Status
    getStatus():stateTile{
        return this._status;
    }

    setStatus(status:stateTile, timeout?:number):void {
        if (timeout && timeout > 0) {
            let backStatus: stateTile = this._status;
            this.scheduleOnce(() => {
                this.setStatus(backStatus);
            }, timeout);
        }
        this._status = status;
    }

    // # Type
    setType(type:stateGiftType):void {
        this._type = type;
        let sprite: cc.SpriteFrame = null;
        switch(type) {
            case stateGiftType.horizontal: 
                sprite = this.spriteFrameHorizontal;
            break;

            case stateGiftType.vertical: 
                sprite = this.spriteFrameVertical;
            break;
        }
        this.getComponent(cc.Sprite).spriteFrame = sprite;
    }

    getType():stateGiftType {
        return this._type;
    }
    
    // # Animation
    startRemove():void {
        let animationCompanent: cc.Animation = this.node.getComponent(cc.Animation);
        animationCompanent.play("tileDestroy");
        animationCompanent.on("finished", this.remove, this);

        this.setStatus(stateTile.destroy);
    }

    remove():void {
        // Отписка от анимации
        let animationCompanent: cc.Animation = this.node.getComponent(cc.Animation);
        animationCompanent.off("finished", this.remove, this);

        // Уничтожение
        this.destroy();
    }
}