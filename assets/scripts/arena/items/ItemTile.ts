/**
 * Imports modules
 */
import { stateTileType, stateTileSprite, stateTile } from "../stateEnums";
import { config } from "../Config";

/**
 * Class Constructor
 */
const {ccclass, property} = cc._decorator;

@ccclass
export class ItemTile extends cc.Component {
    /**
     * @values
     */

    // # SpriteFrames
    @property   (cc.SpriteFrame) spriteFrameRed:    cc.SpriteFrame = null; // Тайл: Красная текстура
    @property   (cc.SpriteFrame) spriteFrameOrange: cc.SpriteFrame = null; // Тайл: Оранжевая текстура
    @property   (cc.SpriteFrame) spriteFrameYellow: cc.SpriteFrame = null; // Тайл: Желтая текстура
    @property   (cc.SpriteFrame) spriteFrameGreen:  cc.SpriteFrame = null; // Тайл: Зеленая текстура
    @property   (cc.SpriteFrame) spriteFrameBlue:   cc.SpriteFrame = null; // Тайл: Голубая текстура
    @property   (cc.SpriteFrame) spriteFramePurple: cc.SpriteFrame = null; // Тайл: Фиолетовая текстура

    // # Public
    public isTile: boolean = true;
    public isGift: boolean = false;

    // # Private
    private _type: stateTileType;
    private _status: stateTile = stateTile.spawning;
    private _lasttime: number = 0;

    /**
     * @functions
     */
    // # Base
    onLoad():void {

    }
    onDestroy():void {
        this.node.destroy();
    }

    // # Main
    spawn(forceAnimation:boolean = false):void {
        // Запуск анимации
        if (!forceAnimation) this.runAnimation("spawn");

        this.setStatus(stateTile.active);
    }

    // # Type
    setType(type:stateTileType):void {
        this._type = type;
        this.getComponent(cc.Sprite).spriteFrame = this[stateTileSprite[this._type]];
    }

    getType():stateTileType {
        return this._type;
    }

    // # Position
    setPos(pos:cc.Vec2, force: boolean = false):number {
        if (force) {
            let abcX: number = Math.abs(this.node.position.x - pos.x);
            let abcY: number = Math.abs(this.node.position.y - pos.y);
            if (abcY > abcX) abcX = abcY;

            let time: number = Math.round(abcX / config.cellSize) * config.tileDropSpeed;
            this.setStatus(stateTile.moving, time);
            let animation: cc.Action = cc.moveTo(time, pos);
            this.node.runAction(animation);

            this.scheduleOnce(() => {
                this.setStatus(stateTile.active);
            }, time);

            return time;
        }

        this.node.setPosition(pos);
        return 0;
    }

    // # Animation
    runAnimation(animType:string):void {
        if (this.getStatus() != stateTile.active) return;
        let animationCompanent: cc.Animation = this.node.getComponent(cc.Animation);
        switch(animType) {
            // Создание
            case "spawn":
                this.setStatus(stateTile.destroy);
                animationCompanent.play("tileSpawn");
                break;
            // Удаление
            case "destroy":
                animationCompanent.play("tileDestroy");
                animationCompanent.on("finished", this.remove, this);
                break;
        }
    }

    // # Status
    getStatus():stateTile{
        return this._status;
    }
    setStatus(status:stateTile, timeout?:number):void {
        if (timeout && timeout > 0) {
            let backStatus: stateTile = this._status;
            this.scheduleOnce(() => {
                this.setStatus(backStatus);
            }, timeout);
        }
        this._status = status;
    }

    // # Base
    update(dt):void {

    }
    remove():void {
        // Отписка от анимации
        let animationCompanent: cc.Animation = this.node.getComponent(cc.Animation);
        animationCompanent.off("finished", this.remove, this);

        // Уничтожение
        this.destroy();
    }
}