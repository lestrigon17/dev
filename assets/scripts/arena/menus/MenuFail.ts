/**
 * Imports modules
 */

import { config } from "../config";
import { EventName } from "../EventName";

/**
 * Class Constructor
 */
const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuFail extends cc.Component {
    /**********************************
     **** @property
     **********************************/
    // Кнопки
    @property (cc.Node) buttonContinue: cc.Node = null; // Продолжить игру
    @property (cc.Node) buttonNewGame:  cc.Node = null; // Новая игра
    @property (cc.Node) buttonExit:     cc.Node = null; // Кнопка выхода

    /**********************************
     **** @functions
     **********************************/
    onLoad():void {
        // Global
        cc.systemEvent.on(EventName.MenuFail, this.show, this);

        // Buttons
        this.buttonNewGame.on(cc.Node.EventType.TOUCH_START, this.fButtonNew, this);
        this.buttonExit.on(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
        this.buttonContinue.on(cc.Node.EventType.TOUCH_START, this.fButtonContinue, this);

        // Сокрытие нода
        this.node.active = false;
    }

    onDestroy():void {
        // Global
        cc.systemEvent.off(EventName.MenuFail, this.show, this);

        // Buttons
        this.buttonNewGame.off(cc.Node.EventType.TOUCH_START, this.fButtonNew, this);
        this.buttonExit.off(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
        this.buttonContinue.off(cc.Node.EventType.TOUCH_START, this.fButtonContinue, this);
    }

    // Закрытие и открытие окна
    show():void {
        this.node.active = true;
        this.getComponent(cc.Animation).play("menuFadeIN");
    }

    onClose():void {
        this.getComponent(cc.Animation).off("finished", this.onClose, this);
        this.node.active = false;
    }

    close():void {
        this.getComponent(cc.Animation).play("menuFadeOUT");
        this.getComponent(cc.Animation).on("finished", this.onClose, this);
    }

    // Функции кнопок
    fButtonNew(event):void {
        this.close();
        cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName.GameStart, true));
    }

    fButtonExit(event):void {
        cc.game.end();
    }

    fButtonContinue(event):void {
        this.close();
        cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName.GameContinue, true));
    }


    update(dt):void {}
}