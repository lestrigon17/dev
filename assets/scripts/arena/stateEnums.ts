/**
 * Tile types
 */
export enum stateTileType {
    red,
    orange,
    yellow,
    green,
    blue,
    purple
}

export function getRandomTileType(): stateTileType[] {
    let types:stateTileType[] = [];
    let typesCount = Object.keys(stateTileType).length/2;

    for (var i = 0; i < typesCount; i ++) {
        types.push(stateTileType[stateTileType[i]]);
    }

    return types;
}

/**
 * Tile sprite of type
 */
export var stateTileSprite = {
    [stateTileType.red]:    "spriteFrameRed",
    [stateTileType.orange]: "spriteFrameOrange",
    [stateTileType.yellow]: "spriteFrameYellow",
    [stateTileType.green]:  "spriteFrameGreen",
    [stateTileType.blue]:   "spriteFrameBlue",
    [stateTileType.purple]: "spriteFramePurple",
}

/**
 * Tile status
 */
export enum stateTile {
    spawning,
    active,
    moving,
    destroy,
    busy
}

/**
 * Cell status
 */
export enum stateCell {
    banned,
    blocked,
    active,
    hole,
    destroy,
    swipe
}

/**
 * Gift  type
 */

export enum stateGiftType {
    horizontal = 100,
    vertical
}