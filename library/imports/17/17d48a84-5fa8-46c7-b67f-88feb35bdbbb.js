"use strict";
cc._RF.push(module, '17d48qEX6hGx7Z/iP6zW9u7', 'MenuFail');
// scripts/arena/menus/MenuFail.ts

"use strict";
/**
 * Imports modules
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventName_1 = require("../EventName");
/**
 * Class Constructor
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MenuFail = /** @class */ (function (_super) {
    __extends(MenuFail, _super);
    function MenuFail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**********************************
         **** @property
         **********************************/
        // Кнопки
        _this.buttonContinue = null; // Продолжить игру
        _this.buttonNewGame = null; // Новая игра
        _this.buttonExit = null; // Кнопка выхода
        return _this;
    }
    /**********************************
     **** @functions
     **********************************/
    MenuFail.prototype.onLoad = function () {
        // Global
        cc.systemEvent.on(EventName_1.EventName.MenuFail, this.show, this);
        // Buttons
        this.buttonNewGame.on(cc.Node.EventType.TOUCH_START, this.fButtonNew, this);
        this.buttonExit.on(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
        this.buttonContinue.on(cc.Node.EventType.TOUCH_START, this.fButtonContinue, this);
        // Сокрытие нода
        this.node.active = false;
    };
    MenuFail.prototype.onDestroy = function () {
        // Global
        cc.systemEvent.off(EventName_1.EventName.MenuFail, this.show, this);
        // Buttons
        this.buttonNewGame.off(cc.Node.EventType.TOUCH_START, this.fButtonNew, this);
        this.buttonExit.off(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
        this.buttonContinue.off(cc.Node.EventType.TOUCH_START, this.fButtonContinue, this);
    };
    // Закрытие и открытие окна
    MenuFail.prototype.show = function () {
        this.node.active = true;
        this.getComponent(cc.Animation).play("menuFadeIN");
    };
    MenuFail.prototype.onClose = function () {
        this.getComponent(cc.Animation).off("finished", this.onClose, this);
        this.node.active = false;
    };
    MenuFail.prototype.close = function () {
        this.getComponent(cc.Animation).play("menuFadeOUT");
        this.getComponent(cc.Animation).on("finished", this.onClose, this);
    };
    // Функции кнопок
    MenuFail.prototype.fButtonNew = function (event) {
        this.close();
        cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName_1.EventName.GameStart, true));
    };
    MenuFail.prototype.fButtonExit = function (event) {
        cc.game.end();
    };
    MenuFail.prototype.fButtonContinue = function (event) {
        this.close();
        cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName_1.EventName.GameContinue, true));
    };
    MenuFail.prototype.update = function (dt) { };
    __decorate([
        property(cc.Node)
    ], MenuFail.prototype, "buttonContinue", void 0);
    __decorate([
        property(cc.Node)
    ], MenuFail.prototype, "buttonNewGame", void 0);
    __decorate([
        property(cc.Node)
    ], MenuFail.prototype, "buttonExit", void 0);
    MenuFail = __decorate([
        ccclass
    ], MenuFail);
    return MenuFail;
}(cc.Component));
exports.default = MenuFail;

cc._RF.pop();