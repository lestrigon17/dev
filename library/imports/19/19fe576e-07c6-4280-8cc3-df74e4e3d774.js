"use strict";
cc._RF.push(module, '19fe5duB8ZCgIzD33Tk49d0', 'ItemGift');
// scripts/arena/items/ItemGift.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Imports modules
 */
var stateEnums_1 = require("../stateEnums");
var Config_1 = require("../Config");
/**
 * Class Constructor
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemGift = /** @class */ (function (_super) {
    __extends(ItemGift, _super);
    function ItemGift() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // # Property
        _this.spriteFrameHorizontal = null; // Текстура горизонтального подарка
        _this.spriteFrameVertical = null; // Текстура вертикального подарка
        // # Public
        _this.isGift = true;
        _this.isTile = false;
        // # Private
        _this._type = null;
        _this._status = stateEnums_1.stateTile.active;
        return _this;
    }
    ItemGift.prototype.onLoad = function () {
    };
    ItemGift.prototype.onDestroy = function () {
        this.node.destroy();
    };
    ItemGift.prototype.IsGift = function () {
        return true;
    };
    // # Position
    ItemGift.prototype.setPos = function (pos, force) {
        var _this = this;
        if (force === void 0) { force = false; }
        if (force) {
            var abcX = Math.abs(this.node.position.x - pos.x);
            var abcY = Math.abs(this.node.position.y - pos.y);
            if (abcY > abcX)
                abcX = abcY;
            var time = Math.round(abcX / Config_1.config.cellSize) * Config_1.config.tileDropSpeed;
            this.setStatus(stateEnums_1.stateTile.moving, time);
            var animation = cc.moveTo(time, pos).easing(cc.easeCircleActionInOut());
            this.node.runAction(animation);
            this.scheduleOnce(function () {
                _this.setStatus(stateEnums_1.stateTile.active);
            }, time);
            return time;
        }
        this.node.setPosition(pos);
        return 0;
    };
    // # Status
    ItemGift.prototype.getStatus = function () {
        return this._status;
    };
    ItemGift.prototype.setStatus = function (status, timeout) {
        var _this = this;
        if (timeout && timeout > 0) {
            var backStatus_1 = this._status;
            this.scheduleOnce(function () {
                _this.setStatus(backStatus_1);
            }, timeout);
        }
        this._status = status;
    };
    // # Type
    ItemGift.prototype.setType = function (type) {
        this._type = type;
        var sprite = null;
        switch (type) {
            case stateEnums_1.stateGiftType.horizontal:
                sprite = this.spriteFrameHorizontal;
                break;
            case stateEnums_1.stateGiftType.vertical:
                sprite = this.spriteFrameVertical;
                break;
        }
        this.getComponent(cc.Sprite).spriteFrame = sprite;
    };
    ItemGift.prototype.getType = function () {
        return this._type;
    };
    // # Animation
    ItemGift.prototype.startRemove = function () {
        var animationCompanent = this.node.getComponent(cc.Animation);
        animationCompanent.play("tileDestroy");
        animationCompanent.on("finished", this.remove, this);
        this.setStatus(stateEnums_1.stateTile.destroy);
    };
    ItemGift.prototype.remove = function () {
        // Отписка от анимации
        var animationCompanent = this.node.getComponent(cc.Animation);
        animationCompanent.off("finished", this.remove, this);
        // Уничтожение
        this.destroy();
    };
    __decorate([
        property(cc.SpriteFrame)
    ], ItemGift.prototype, "spriteFrameHorizontal", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemGift.prototype, "spriteFrameVertical", void 0);
    ItemGift = __decorate([
        ccclass
    ], ItemGift);
    return ItemGift;
}(cc.Component));
exports.ItemGift = ItemGift;

cc._RF.pop();