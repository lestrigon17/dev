"use strict";
cc._RF.push(module, '8f36e1ZxYtHnYHoudem9rLU', 'ItemTile');
// scripts/arena/items/ItemTile.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Imports modules
 */
var stateEnums_1 = require("../stateEnums");
var Config_1 = require("../Config");
/**
 * Class Constructor
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemTile = /** @class */ (function (_super) {
    __extends(ItemTile, _super);
    function ItemTile() {
        /**
         * @values
         */
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // # SpriteFrames
        _this.spriteFrameRed = null; // Тайл: Красная текстура
        _this.spriteFrameOrange = null; // Тайл: Оранжевая текстура
        _this.spriteFrameYellow = null; // Тайл: Желтая текстура
        _this.spriteFrameGreen = null; // Тайл: Зеленая текстура
        _this.spriteFrameBlue = null; // Тайл: Голубая текстура
        _this.spriteFramePurple = null; // Тайл: Фиолетовая текстура
        // # Public
        _this.isTile = true;
        _this.isGift = false;
        _this._status = stateEnums_1.stateTile.spawning;
        _this._lasttime = 0;
        return _this;
    }
    /**
     * @functions
     */
    // # Base
    ItemTile.prototype.onLoad = function () {
    };
    ItemTile.prototype.onDestroy = function () {
        this.node.destroy();
    };
    // # Main
    ItemTile.prototype.spawn = function (forceAnimation) {
        if (forceAnimation === void 0) { forceAnimation = false; }
        // Запуск анимации
        if (!forceAnimation)
            this.runAnimation("spawn");
        this.setStatus(stateEnums_1.stateTile.active);
    };
    // # Type
    ItemTile.prototype.setType = function (type) {
        this._type = type;
        this.getComponent(cc.Sprite).spriteFrame = this[stateEnums_1.stateTileSprite[this._type]];
    };
    ItemTile.prototype.getType = function () {
        return this._type;
    };
    // # Position
    ItemTile.prototype.setPos = function (pos, force) {
        var _this = this;
        if (force === void 0) { force = false; }
        if (force) {
            var abcX = Math.abs(this.node.position.x - pos.x);
            var abcY = Math.abs(this.node.position.y - pos.y);
            if (abcY > abcX)
                abcX = abcY;
            var time = Math.round(abcX / Config_1.config.cellSize) * Config_1.config.tileDropSpeed;
            this.setStatus(stateEnums_1.stateTile.moving, time);
            var animation = cc.moveTo(time, pos);
            this.node.runAction(animation);
            this.scheduleOnce(function () {
                _this.setStatus(stateEnums_1.stateTile.active);
            }, time);
            return time;
        }
        this.node.setPosition(pos);
        return 0;
    };
    // # Animation
    ItemTile.prototype.runAnimation = function (animType) {
        if (this.getStatus() != stateEnums_1.stateTile.active)
            return;
        var animationCompanent = this.node.getComponent(cc.Animation);
        switch (animType) {
            // Создание
            case "spawn":
                this.setStatus(stateEnums_1.stateTile.destroy);
                animationCompanent.play("tileSpawn");
                break;
            // Удаление
            case "destroy":
                animationCompanent.play("tileDestroy");
                animationCompanent.on("finished", this.remove, this);
                break;
        }
    };
    // # Status
    ItemTile.prototype.getStatus = function () {
        return this._status;
    };
    ItemTile.prototype.setStatus = function (status, timeout) {
        var _this = this;
        if (timeout && timeout > 0) {
            var backStatus_1 = this._status;
            this.scheduleOnce(function () {
                _this.setStatus(backStatus_1);
            }, timeout);
        }
        this._status = status;
    };
    // # Base
    ItemTile.prototype.update = function (dt) {
    };
    ItemTile.prototype.remove = function () {
        // Отписка от анимации
        var animationCompanent = this.node.getComponent(cc.Animation);
        animationCompanent.off("finished", this.remove, this);
        // Уничтожение
        this.destroy();
    };
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFrameRed", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFrameOrange", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFrameYellow", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFrameGreen", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFrameBlue", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ItemTile.prototype, "spriteFramePurple", void 0);
    ItemTile = __decorate([
        ccclass
    ], ItemTile);
    return ItemTile;
}(cc.Component));
exports.ItemTile = ItemTile;

cc._RF.pop();