"use strict";
cc._RF.push(module, '8f3c4r7Hf5CS5Chr8QogzTn', 'Config');
// scripts/arena/Config.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
        // Размерность поля
        this.fieldSizeX = 10; // Ось: Х
        this.fieldSizeY = 11; // Ось: У
        // Размер канвы
        this.canvasSizeX = 1200; // Ось: Х
        this.canvasSizeY = 690; // Ось: У
        // Размер тайлов и клеток
        this.tileSize = 30; // Размер спрайта тайла
        this.cellSize = 62; // Размер ячейки сетки
        // Скорость tile'ov
        this.tileDropSpeed = 0.200; // с
        this.tileDestroySpeed = 0.300; // с
        this.tileDissolveSpeed = 0.100; // с
        this.doubleClickTimeout = 500; // мс
    }
    return Config;
}());
exports.config = new Config();

cc._RF.pop();