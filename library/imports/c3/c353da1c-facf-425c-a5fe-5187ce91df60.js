"use strict";
cc._RF.push(module, 'c353doc+s9CXKX+UYfOkd9g', 'EventName');
// scripts/arena/EventName.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventName = {
    // Game
    GameStart: "gameStart",
    GamePause: "gamePause",
    GameContinue: "gameContinue",
    GameLose: "gameLose",
    GameWin: "gameWin",
    // Menus
    MenuFail: "menuFail",
    MenuNewGame: "menuNewGame",
    MenuWinner: "menuWinner"
};

cc._RF.pop();