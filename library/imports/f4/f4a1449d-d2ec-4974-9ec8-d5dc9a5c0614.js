"use strict";
cc._RF.push(module, 'f4a14Sd0uxJdJ7I1dyaXAYU', 'position');
// scripts/arena/functions/position.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Coords = /** @class */ (function () {
    function Coords(column, row) {
        this.column = 0; // x
        this.row = 0; // y
        this.column = column;
        this.row = row;
    }
    return Coords;
}());
exports.Coords = Coords;
exports.CoordAxis = {
    row: "row",
    column: "column"
};

cc._RF.pop();