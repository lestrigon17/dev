(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/Config.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '8f3c4r7Hf5CS5Chr8QogzTn', 'Config', __filename);
// scripts/arena/Config.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
        // Размерность поля
        this.fieldSizeX = 10; // Ось: Х
        this.fieldSizeY = 11; // Ось: У
        // Размер канвы
        this.canvasSizeX = 1200; // Ось: Х
        this.canvasSizeY = 690; // Ось: У
        // Размер тайлов и клеток
        this.tileSize = 30; // Размер спрайта тайла
        this.cellSize = 62; // Размер ячейки сетки
        // Скорость tile'ov
        this.tileDropSpeed = 0.200; // с
        this.tileDestroySpeed = 0.300; // с
        this.tileDissolveSpeed = 0.100; // с
        this.doubleClickTimeout = 500; // мс
    }
    return Config;
}());
exports.config = new Config();

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Config.js.map
        