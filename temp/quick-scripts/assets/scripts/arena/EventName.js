(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/EventName.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'c353doc+s9CXKX+UYfOkd9g', 'EventName', __filename);
// scripts/arena/EventName.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventName = {
    // Game
    GameStart: "gameStart",
    GamePause: "gamePause",
    GameContinue: "gameContinue",
    GameLose: "gameLose",
    GameWin: "gameWin",
    // Menus
    MenuFail: "menuFail",
    MenuNewGame: "menuNewGame",
    MenuWinner: "menuWinner"
};

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=EventName.js.map
        