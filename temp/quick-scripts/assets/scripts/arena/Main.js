(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/Main.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '03301Z0nnVCarCqiD2rwdpT', 'Main', __filename);
// scripts/arena/Main.ts

"use strict";
/**
 * Imports modules
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("./config");
var EventName_1 = require("./EventName");
var ItemTile_1 = require("./items/ItemTile");
var position_1 = require("./functions/position");
var ItemGift_1 = require("./items/ItemGift");
// Enums
var stateEnums_1 = require("./stateEnums");
/**
 * enum export
 */
var SceneStatus;
(function (SceneStatus) {
    SceneStatus[SceneStatus["spawning"] = 0] = "spawning";
    SceneStatus[SceneStatus["isMoving"] = 1] = "isMoving";
    SceneStatus[SceneStatus["isSwiping"] = 2] = "isSwiping";
    SceneStatus[SceneStatus["isDropping"] = 3] = "isDropping";
    SceneStatus[SceneStatus["exploding"] = 4] = "exploding";
    SceneStatus[SceneStatus["active"] = 5] = "active";
    SceneStatus[SceneStatus["menu"] = 6] = "menu";
})(SceneStatus = exports.SceneStatus || (exports.SceneStatus = {}));
/**
 * Class Constructor
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ArenaController = /** @class */ (function (_super) {
    __extends(ArenaController, _super);
    function ArenaController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**********************************
         **** @property
         **********************************/
        // # SPRITE NODE
        _this.spriteChooser = null; // Обводка вокруг выбранного тайла
        _this.spriteRequire = null; // Спрайт цель внутри задания
        // # SpriteFrames
        _this.spriteFrameRed = null; // Тайл: Красная текстура
        _this.spriteFrameOrange = null; // Тайл: Оранжевая текстура
        _this.spriteFrameYellow = null; // Тайл: Желтая текстура
        _this.spriteFrameGreen = null; // Тайл: Зеленая текстура
        _this.spriteFrameBlue = null; // Тайл: Голубая текстура
        _this.spriteFramePurple = null; // Тайл: Фиолетовая текстура
        // # LABEL
        _this.labelMoves = null; // Counter шагов
        _this.labelRequire = null; // Counter цели
        // # Buttons
        _this.buttonBooster = null; // Кнопка : Бустер
        // # json configuration
        _this.jsonLevelData = null; // Информация о поле
        // # prefabs
        _this.itemGiftPrefab = null; // молния
        _this.itemTilePrefab = null; // Тайл
        _this.cellItemHole = null; // Дырка на поле
        _this.cellItemBlock = null; // Блок для падения тайла
        // # Tile parent
        _this.arenaTiles = null; // Родительский элемент для тайлов
        _this.arenaHoles = null; // Родительский элемент для дырок
        _this.arenaBlocks = null; // Родительский элемент для Блока
        /**********************************
         **** @private
         **********************************/
        // # LOCAL VALUES
        // Кол-Во шагов
        _this._moves = 0; // Кол-во возможных шагов
        _this._movesNow = 0; // Сколько сделали шагов
        // ЦЕЛЬ ИГРЫ
        _this._requireType = null; // Требуемый тип тайлов
        _this._requirePicked = 0; // Кол-во необходимо собрать
        _this._requirePickedNow = 0; // Сколько уже собрали
        // БУСТЕР
        _this._boosterIsActive = false; // Активирован ли бустер
        _this._boosterIsUsed = false; // использован ли бустер
        // Работа с тачем
        _this._touchTilePos = new cc.Vec2(0, 0); // Предыдущая точка которую подобрал тач
        _this._touchTileNode = null; // Предыдущая точка которую подобрал тач
        _this._touchLastClick = 0;
        // Массив точек
        _this._subMeta = [[]]; // Виртуальная мета поля
        _this._meta = [[]]; // Видимая мета поля
        _this._metaSpawned = false; // Проверка на то, существует ли мета
        // Состояние сцены
        _this._sceneStatus = SceneStatus.spawning;
        // таблица спавна предыдущих точек при спавне новых
        _this._spawnerData = []; // Таблица с предыдущие выпавшими тайлами
        // Отступы на поле
        _this._arenaPaddingX = 0;
        _this._arenaPaddingY = 0;
        return _this;
    }
    /**********************************
     **** @functions
     **********************************/
    // События
    ArenaController.prototype.onLoad = function () {
        // Скрытие chooser
        this.spriteChooser.node.active = false;
        // Глобальные события
        cc.systemEvent.on(EventName_1.EventName.GameStart, this.gameStart, this); // Старт игры
        cc.systemEvent.on(EventName_1.EventName.GameContinue, this.gameContinue, this); // Продолжение игры
        // Работа с TOUCH касаниями
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        // Кнопки на игровом поле
        this.buttonBooster.on(cc.Node.EventType.TOUCH_START, this.fButtonBooser, this);
    };
    ArenaController.prototype.onDestroy = function () {
        cc.systemEvent.off(EventName_1.EventName.GameStart, this.gameStart, this);
        cc.systemEvent.off(EventName_1.EventName.GameContinue, this.gameContinue, this);
        this.buttonBooster.off(cc.Node.EventType.TOUCH_START, this.fButtonBooser, this);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    };
    /*******************************************
     **** Перебор всего поля
     *******************************************/
    ArenaController.prototype.getEveryCell = function (callback, fromEnd, byColumn) {
        if (fromEnd === void 0) { fromEnd = false; }
        if (byColumn === void 0) { byColumn = false; }
        var columnCount = config_1.config.fieldSizeX;
        var rowCount = config_1.config.fieldSizeY;
        if (fromEnd) {
            if (byColumn) {
                for (var column = 0; column < columnCount; column++) {
                    for (var row = rowCount - 1; row >= 0; row--) {
                        callback(column, row);
                    }
                }
                return;
            }
            for (var row = rowCount - 1; row >= 0; row--) {
                for (var column = 0; column < columnCount; column++) {
                    callback(column, row);
                }
            }
            return;
        }
        if (byColumn) {
            for (var column = 0; column < columnCount; column++) {
                for (var row = 0; row < rowCount; row++) {
                    callback(column, row);
                }
            }
            return;
        }
        for (var row = 0; row < rowCount; row++) {
            for (var column = 0; column < columnCount; column++) {
                callback(column, row);
            }
        }
    };
    /*******************************************
     **** Работа с TOUCH событиями
     *******************************************/
    ArenaController.prototype.onTouchStart = function (event) {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        var coordNow = this.getCoordFromPosition(event.getLocation());
        var coordLast = this.getCoordFromPosition(this._touchTilePos);
        var cursorCell = this.getCell(coordNow);
        var isDoubleClicked = false;
        if (this._touchLastClick + config_1.config.doubleClickTimeout > Date.now()) {
            isDoubleClicked = true;
        }
        this._touchLastClick = Date.now();
        if (this._boosterIsActive && isDoubleClicked) {
            if (cursorCell.node.isTile) {
                this.tileRemove(coordNow);
            }
            if (cursorCell.node.isGift) {
                this.giftExplode(coordNow);
            }
            this.gameAddMoves();
            this.clearTouchTile();
            return;
        }
        if (this._touchTileNode == null) {
            if (cursorCell.status == stateEnums_1.stateCell.active) {
                this._touchTilePos = event.getLocation();
                this._touchTileNode = cursorCell.node;
            }
            return;
        }
        if (cursorCell.status != stateEnums_1.stateCell.active || this._touchTileNode == cursorCell.node) {
            if (isDoubleClicked && cursorCell.node.isGift) {
                this.giftExplode(coordNow);
            }
            this._touchTileNode = null;
            this._touchTilePos = cc.v2(0, 0);
            return;
        }
        var canSwipe = this.tileCanSwipe(coordLast, coordNow);
        if (canSwipe == false) {
            this._touchTileNode = cursorCell.node;
            this._touchTilePos = event.getLocation();
            return;
        }
        var response = this.metaMoveTile(coordLast, coordNow);
        if (response) {
            this.gameAddMoves();
        }
        else {
            this.tileFakeSwipe(coordLast, coordNow);
        }
        this.clearTouchTile();
    };
    ArenaController.prototype.onTouchMove = function (event) {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        if (this._touchTileNode == null) {
            return;
        }
        var xDelta = Math.abs(this._touchTilePos.x - event.getLocationX());
        var yDelta = Math.abs(this._touchTilePos.y - event.getLocationY());
        var maxDelta = config_1.config.cellSize / 2;
        var coordStart = this.getCoordFromPosition(this._touchTilePos);
        var coordEnd = this.getCoordFromPosition(event.getLocation());
        if (xDelta > maxDelta && yDelta > maxDelta) {
            this.clearTouchTile();
            return;
        }
        if (coordStart != coordEnd) {
            var canSwipe = this.tileCanSwipe(coordStart, coordEnd);
            if (canSwipe == false) {
                return;
            }
            var response = this.metaMoveTile(coordStart, coordEnd);
            if (response) {
                this.gameAddMoves();
            }
            else {
                this.tileFakeSwipe(coordStart, coordEnd);
            }
            this.clearTouchTile();
        }
    };
    ArenaController.prototype.clearTouchTile = function () {
        this._touchTileNode = null;
        this._touchTilePos = cc.v2(0, 0);
    };
    ArenaController.prototype.onTouchEnd = function (event) {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        var lastClickWithTimeOut = this._touchLastClick + config_1.config.doubleClickTimeout * 2;
        if (lastClickWithTimeOut > Date.now()) {
            return;
        }
        this.clearTouchTile();
    };
    /*******************************************
     **** Работа с игровой механикой
     *******************************************/
    /**
     * Генерация ячеек на поле и суб-поле
     * Заполнение таблицы пустотой
     */
    ArenaController.prototype.genMeta = function () {
        var _this = this;
        if (this._metaSpawned) {
            return;
        }
        this.getEveryCell(function (column, row) {
            if (!Array.isArray(_this._meta[row])) {
                _this._meta[row] = [];
            }
            if (!Array.isArray(_this._subMeta[row])) {
                _this._subMeta[row] = [];
            }
            _this._meta[row][column] = {
                status: stateEnums_1.stateCell.blocked,
                node: null
            };
            _this._subMeta[row][column] = null;
        });
        this._metaSpawned = true;
    };
    /**
     * Очищение поля
     */
    ArenaController.prototype.gameClear = function () {
        var _this = this;
        if (!this._metaSpawned)
            return;
        this.getEveryCell(function (column, row) {
            var cell = _this.getCell(new position_1.Coords(column, row));
            if (cell.status != stateEnums_1.stateCell.active) {
                return;
            }
            cell.node.destroy();
            cell.status = stateEnums_1.stateCell.hole;
        });
    };
    /**
     * Иниаициализация поля
     * Установка и генерация тайлов
     */
    ArenaController.prototype.gameBuildArena = function () {
        var _this = this;
        var fieldData = this.jsonLevelData.json;
        var oldType = null;
        var downType = null;
        this.getEveryCell(function (column, row) {
            var coordCell = new position_1.Coords(column, row);
            var tileType = fieldData.field_dots[row][column];
            var cell = _this.getCell(coordCell);
            var metaCell = _this._subMeta[row][column];
            if (tileType == -1) {
                _this.cellCreateBGHole(coordCell);
            }
            else {
                _this.cellCreateBGBlock(coordCell);
            }
            switch (tileType) {
                case -1:
                    metaCell = null;
                    cell.status = stateEnums_1.stateCell.banned;
                    break;
                case 0:
                    metaCell = null;
                    cell.status = stateEnums_1.stateCell.hole;
                    break;
                case 99:
                    if (row > 0) {
                        var downCell = _this.getCell(new position_1.Coords(column, row - 1));
                        if (downCell.status == stateEnums_1.stateCell.active) {
                            downType = downCell.node.getType();
                        }
                    }
                    var randomType = _this.tileGetRandomType([oldType, downType]);
                    _this.tileCreate(coordCell, randomType, true);
                    oldType = randomType;
                    break;
                case 100:
                    _this.giftCreate(stateEnums_1.stateGiftType.horizontal, coordCell);
                    break;
                case 101:
                    _this.giftCreate(stateEnums_1.stateGiftType.vertical, coordCell);
                    break;
                default:
                    var itemType = stateEnums_1.stateTileType[stateEnums_1.stateTileType[tileType]];
                    _this.tileCreate(coordCell, itemType, true);
                    oldType = itemType;
                    break;
            }
        });
    };
    /**
     * Начало новой игры
     */
    ArenaController.prototype.gameStart = function () {
        this.setSceneStatus(SceneStatus.spawning);
        this.gameClear();
        this.genMeta(); // генерация меты поля
        this.gameBuildArena(); // генерация тайлов на поле
        /**
         * Установка целей
         */
        var jsonData = this.jsonLevelData.json;
        this._moves = jsonData.level_moves;
        this._requirePicked = jsonData.level_require;
        this._requireType = stateEnums_1.stateTileType[stateEnums_1.stateTileType[jsonData.level_require_type]];
        this._movesNow = 0;
        this._requirePickedNow = 0;
        this._boosterIsActive = false;
        this._boosterIsUsed = false;
        this.setSceneStatus(SceneStatus.active);
    };
    /**
     * Продолжение старой игры
     */
    ArenaController.prototype.gameContinue = function () {
        this._moves += 5;
        this.setSceneStatus(SceneStatus.active);
    };
    /*******************************************
     **** Работа с ячейками
     *******************************************/
    /**
     * Блокировка ячейки для каких-либо манипуляций
     */
    ArenaController.prototype.banCell = function (coords) {
        this._meta[coords.row][coords.column] = {
            status: stateEnums_1.stateCell.banned,
            node: null
        };
    };
    /**
     * Получение ячейки с содержимым
     */
    ArenaController.prototype.getCell = function (coords) {
        if (coords.column < 0 ||
            coords.row < 0 ||
            coords.column >= config_1.config.fieldSizeX ||
            coords.row >= config_1.config.fieldSizeY) {
            return { status: stateEnums_1.stateCell.banned, node: null };
        }
        return this._meta[coords.row][coords.column];
    };
    /**
     * Добавление в ячейку тайла
     * @param node - Нод от тайла
     */
    ArenaController.prototype.addCellItem = function (node, coords) {
        if (node instanceof ItemTile_1.ItemTile) {
            this._subMeta[coords.row][coords.column] = node.getType();
        }
        else {
            this._subMeta[coords.row][coords.column] = null;
        }
        this._meta[coords.row][coords.column] = {
            status: stateEnums_1.stateCell.active,
            node: node
        };
    };
    /**
     * Установка статуса для конкретной ячейки на пле
     * @param status - Требуемый статус
     * @param backupTimeout - На какое время выставляется статус. 0 - без возвратного таймера
     */
    ArenaController.prototype.setCellStatus = function (coords, status, backupTimeout) {
        if (backupTimeout === void 0) { backupTimeout = 0; }
        var cell = this.getCell(coords);
        var oldStatus = cell.status;
        cell.status = status;
        if (backupTimeout == 0) {
            return;
        }
        this.scheduleOnce(function () {
            cell.status = oldStatus;
        }, backupTimeout);
    };
    // # Работа с виртуальным полем
    ArenaController.prototype.cellCreateBGHole = function (coordCreate) {
        return;
        /*// Создание нода
        let node: cc.Node = cc.instantiate(this.cellItemHole);
        // Добавление на сцену
        this.arenaBlocks.addChild(node)
        // Установка позиции
        let pos: cc.Vec2 = this.getPositionFromCoords(coordCreate);
        node.setPosition(pos);

        return node;*/
    };
    ArenaController.prototype.cellCreateBGBlock = function (coordCreate) {
        var node = cc.instantiate(this.cellItemBlock);
        this.arenaHoles.addChild(node);
        var pos = this.getPositionFromCoords(coordCreate);
        node.setPosition(pos);
        return node;
    };
    /**
     * Проверка, можем ли мы свайпнуть точки между собой и создаст ли это комбинацию
     */
    ArenaController.prototype.metaMoveTile = function (coordStart, coordEnd) {
        var _this = this;
        // CElLS
        var data = [
            {
                cell: this.getCell(coordStart),
                meta: this._subMeta[coordStart.row][coordStart.column],
                coordStart: coordStart,
                coordEnd: coordEnd
            },
            {
                cell: this.getCell(coordEnd),
                meta: this._subMeta[coordEnd.row][coordEnd.column],
                coordStart: coordEnd,
                coordEnd: coordStart
            }
        ];
        var haveGift = false;
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var value = data_1[_i];
            if (value.cell.node.isGift) {
                haveGift = true;
            }
            this._subMeta[value.coordEnd.row][value.coordEnd.column] = value.meta;
        }
        // FIND COMBINATIONS
        var haveSuccessCombination = false;
        var combinations = [
            {
                response: null,
                coords: coordStart
            },
            {
                response: null,
                coords: coordEnd
            },
        ];
        for (var _a = 0, combinations_1 = combinations; _a < combinations_1.length; _a++) {
            var combination = combinations_1[_a];
            combination.response = this.metaFindCombinations(combination.coords);
            if (combination.response.found == true) {
                haveSuccessCombination = true;
            }
        }
        for (var _b = 0, data_2 = data; _b < data_2.length; _b++) {
            var value = data_2[_b];
            this._subMeta[value.coordStart.row][value.coordStart.column] = value.meta;
        }
        if (!haveSuccessCombination && !haveGift) {
            return false;
        }
        // ACTIONS
        this.setSceneStatus(SceneStatus.isSwiping, config_1.config.tileDestroySpeed);
        this.tileSwipe(coordStart, coordEnd);
        var _loop_1 = function (value) {
            if (!value.cell.node.isGift) {
                return "continue";
            }
            this_1.scheduleOnce(function () {
                _this.giftExplode(value.coordEnd);
            }, config_1.config.tileDropSpeed);
        };
        var this_1 = this;
        // FOR CUSTOM TILES
        for (var _c = 0, data_3 = data; _c < data_3.length; _c++) {
            var value = data_3[_c];
            _loop_1(value);
        }
        var _loop_2 = function (combination) {
            var response = combination.response;
            if (response.found == false) {
                return "continue";
            }
            this_2.scheduleOnce(function () {
                for (var _i = 0, _a = response.combo; _i < _a.length; _i++) {
                    var coord = _a[_i];
                    _this.tileRemove(coord);
                }
                if (response.count.x == 4) {
                    _this.giftCreate(stateEnums_1.stateGiftType.horizontal, combination.coords);
                    return;
                }
                if (response.count.y == 4) {
                    _this.giftCreate(stateEnums_1.stateGiftType.vertical, combination.coords);
                }
            }, config_1.config.tileDestroySpeed);
        };
        var this_2 = this;
        // DESTROY COMBINATION
        for (var _d = 0, combinations_2 = combinations; _d < combinations_2.length; _d++) {
            var combination = combinations_2[_d];
            _loop_2(combination);
        }
        return true;
    };
    /**
     * Поиск комбинаций у конкретной точки
     * @param type - Тип точки
     */
    ArenaController.prototype.metaFindCombinations = function (coords) {
        var haveCombinations = false;
        var columnCount = 0;
        var rowCount = 0;
        var dotsComboColumn = [];
        var dotsComboRow = [];
        var type = this._subMeta[coords.row][coords.column];
        if (type == null || type > 99) {
            return {
                found: false,
                combo: [],
                count: new cc.Vec2(0, 0)
            };
        }
        for (var direction = -1; direction <= 1; direction += 2) {
            for (var column = coords.column + direction; direction < 0 && column >= 0 || column < config_1.config.fieldSizeX; column += 1 * direction) {
                if (column < 0 || column > config_1.config.fieldSizeX) {
                    break;
                }
                var typeSecond = this._subMeta[coords.row][column];
                if (typeSecond != type || typeSecond == null) {
                    break;
                }
                dotsComboColumn.push(column);
                columnCount++;
            }
        }
        for (var direction = -1; direction <= 1; direction += 2) {
            for (var row = coords.row + direction; direction < 0 && row >= 0 || row < config_1.config.fieldSizeY; row += 1 * direction) {
                if (row < 0 || row > config_1.config.fieldSizeY) {
                    break;
                }
                var typeSecond = this._subMeta[row][coords.column];
                if (typeSecond != type || typeSecond == null) {
                    break;
                }
                dotsComboRow.push(row);
                rowCount++;
            }
        }
        var tileComboStore = [];
        if (dotsComboColumn.length >= 2) {
            for (var _i = 0, dotsComboColumn_1 = dotsComboColumn; _i < dotsComboColumn_1.length; _i++) {
                var column = dotsComboColumn_1[_i];
                tileComboStore.push(new position_1.Coords(column, coords.row));
            }
            haveCombinations = true;
            columnCount += 1;
        }
        if (dotsComboRow.length >= 2) {
            for (var _a = 0, dotsComboRow_1 = dotsComboRow; _a < dotsComboRow_1.length; _a++) {
                var row = dotsComboRow_1[_a];
                tileComboStore.push(new position_1.Coords(coords.column, row));
            }
            haveCombinations = true;
            rowCount += 1;
        }
        if (haveCombinations) {
            tileComboStore.push(coords);
        }
        return {
            found: haveCombinations,
            combo: tileComboStore,
            count: new cc.Vec2(columnCount, rowCount)
        };
    };
    /**
     * Поиск всех возможных комбинаций на поле
     */
    ArenaController.prototype.metaFindAllCombinations = function () {
        var _this = this;
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        var tempComboItems = [];
        var comboLastType = null;
        var comboItems = [[[], null]];
        var comboCount = 0;
        // rows
        this.getEveryCell(function (column, row) {
            var coordCell = new position_1.Coords(column, row);
            var cell = _this.getCell(coordCell);
            var cellItem = cell.node;
            var cellStatus = cell.status;
            if (cellStatus != stateEnums_1.stateCell.active || !cellItem.isTile) {
                if (comboCount >= 3)
                    comboItems.push([tempComboItems, stateEnums_1.stateGiftType.horizontal]);
                tempComboItems = [];
                comboCount = 0;
                comboLastType = null;
                return;
            }
            if (comboLastType == null || comboLastType == cellItem.getType()) {
                comboLastType = cellItem.getType();
                comboCount += 1;
                tempComboItems.push(coordCell);
                return;
            }
            if (comboCount >= 3) {
                comboItems.push([tempComboItems, stateEnums_1.stateGiftType.horizontal]);
            }
            tempComboItems = [];
            tempComboItems.push(coordCell);
            comboCount = 1;
            comboLastType = cellItem.getType();
            return;
        });
        // column
        this.getEveryCell(function (column, row) {
            var coordCell = new position_1.Coords(column, row);
            var cell = _this.getCell(coordCell);
            var cellItem = cell.node;
            var cellStatus = cell.status;
            if (cellStatus != stateEnums_1.stateCell.active || !cellItem.isTile) {
                if (comboCount >= 3) {
                    comboItems.push([tempComboItems, stateEnums_1.stateGiftType.vertical]);
                }
                tempComboItems = [];
                comboCount = 0;
                comboLastType = null;
                return;
            }
            if (comboLastType == null || comboLastType == cellItem.getType()) {
                comboLastType = cellItem.getType();
                comboCount += 1;
                tempComboItems.push(coordCell);
                return;
            }
            if (comboCount >= 3) {
                comboItems.push([tempComboItems, stateEnums_1.stateGiftType.vertical]);
            }
            tempComboItems = [];
            tempComboItems.push(coordCell);
            comboCount = 1;
            comboLastType = cellItem.getType();
            return;
        }, false, true);
        if (comboItems.length >= 2) {
            for (var _i = 0, comboItems_1 = comboItems; _i < comboItems_1.length; _i++) {
                var combItem = comboItems_1[_i];
                var coordStart = combItem[0][0];
                for (var _a = 0, _b = combItem[0]; _a < _b.length; _a++) {
                    var coordCell = _b[_a];
                    this.tileRemove(coordCell);
                }
                if (combItem[0].length == 4) {
                    this.giftCreate(combItem[1], coordStart);
                }
            }
            this.setSceneStatus(SceneStatus.isSwiping, config_1.config.tileDestroySpeed);
        }
    };
    /*******************************************
     **** Работа с гифтами
     *******************************************/
    /**
     * Создание молнии
     * @param type - Тип вертикальный или горизонтальный
     * @param coordCreate - Позиция
     */
    ArenaController.prototype.giftCreate = function (type, coordCreate) {
        var node = cc.instantiate(this.itemGiftPrefab);
        this.arenaTiles.addChild(node);
        var nodeGift = node.getComponent(ItemGift_1.ItemGift);
        nodeGift.setType(type);
        var pos = this.getPositionFromCoords(coordCreate);
        nodeGift.setPos(pos);
        this._subMeta[coordCreate.row][coordCreate.column] = null;
        this.addCellItem(nodeGift, coordCreate);
        return nodeGift;
    };
    ArenaController.prototype.giftExplode = function (coordExplosion) {
        var cell = this.getCell(coordExplosion);
        if (cell.node.IsValid && !cell.node.isGift) {
            return;
        }
        this.setSceneStatus(SceneStatus.exploding);
        var giftType = cell.node.getType();
        if (giftType == stateEnums_1.stateGiftType.horizontal) {
            this.giftCheck(position_1.CoordAxis.row, coordExplosion);
        }
        else {
            this.giftCheck(position_1.CoordAxis.column, coordExplosion);
        }
        this.tileRemove(coordExplosion);
    };
    ArenaController.prototype.giftCheck = function (align, coords) {
        var _this = this;
        var dir = align == position_1.CoordAxis.row && config_1.config.fieldSizeX || config_1.config.fieldSizeY;
        var coordSec = (align == position_1.CoordAxis.row) && position_1.CoordAxis.column || position_1.CoordAxis.row;
        for (var direction = -1; direction <= 1; direction += 2) {
            var _loop_3 = function (coord) {
                if (coord < 0 || coord > dir) {
                    return "break";
                }
                var coordCell = null;
                if (align == position_1.CoordAxis.row) {
                    coordCell = new position_1.Coords(coord, coords.row);
                }
                if (align == position_1.CoordAxis.column) {
                    coordCell = new position_1.Coords(coords.column, coord);
                }
                var cellNext = this_3.getCell(coordCell);
                if (cellNext.status != stateEnums_1.stateCell.active) {
                    return "continue";
                }
                cellNext.status = stateEnums_1.stateCell.destroy;
                var scheduleTimer = config_1.config.tileDissolveSpeed * Math.abs(coords[coordSec] - coord);
                this_3.scheduleOnce(function () {
                    if (cellNext.status != stateEnums_1.stateCell.destroy) {
                        return;
                    }
                    if (cellNext.node.isValid && cellNext.node.isGift) {
                        _this.giftExplode(coordCell);
                        return;
                    }
                    _this.tileRemove(coordCell);
                }, scheduleTimer);
            };
            var this_3 = this;
            for (var coord = coords[coordSec] + direction; direction < 0 && coord >= 0 || coord < dir; coord += direction) {
                var state_1 = _loop_3(coord);
                if (state_1 === "break")
                    break;
            }
        }
    };
    /*******************************************
     **** Работа с тайлами
     *******************************************/
    /**
     * Создание тайла
     * @param force - Требуется ли проигрывать анимацию появления. true - если да
     */
    ArenaController.prototype.tileCreate = function (coordCreate, type, force) {
        if (force === void 0) { force = false; }
        var node = cc.instantiate(this.itemTilePrefab);
        this.arenaTiles.addChild(node);
        var nodeTile = node.getComponent(ItemTile_1.ItemTile);
        nodeTile.spawn(force);
        var pos = this.getPositionFromCoords(coordCreate);
        nodeTile.setPos(pos);
        nodeTile.setType(type);
        this._subMeta[coordCreate.row][coordCreate.column] = type;
        this.addCellItem(nodeTile, coordCreate);
        return nodeTile;
    };
    /**
     * Получение рандомного типа для ячейки
     * @param excludeTypes - Список типов которые запрещены к рассмотрению
     */
    ArenaController.prototype.tileGetRandomType = function (excludeTypes) {
        if (excludeTypes === void 0) { excludeTypes = []; }
        var availableTypes = [];
        for (var _i = 0, _a = stateEnums_1.getRandomTileType(); _i < _a.length; _i++) {
            var type = _a[_i];
            if (excludeTypes.indexOf(type) >= 0) {
                continue;
            }
            availableTypes.push(type);
        }
        var randomID = randomInteger(0, availableTypes.length - 1);
        return availableTypes[randomID];
    };
    /*******************************************
     **** Работа с движением ячеек
     *******************************************/
    /**
     * Вычесление расстояния между двумя ячейками (горизонталь|вертикаль)
     */
    ArenaController.prototype.tileCanSwipe = function (coordStart, coordEnd) {
        var column = Math.abs(coordStart.column - coordEnd.column);
        var row = Math.abs(coordStart.row - coordEnd.row);
        if (column > 1 || row > 1) {
            return false;
        }
        var cell1 = this.getCell(coordStart);
        if (cell1.status != stateEnums_1.stateCell.active) {
            return false;
        }
        var cell2 = this.getCell(coordEnd);
        if (cell2.status != stateEnums_1.stateCell.active) {
            return false;
        }
        var diff = Math.abs(column - row);
        if (diff == 1) {
            return true;
        }
        return false;
    };
    /**
     * Фейковый свайп
     */
    ArenaController.prototype.tileFakeSwipe = function (coordStart, coordEnd) {
        var _this = this;
        var data = [
            {
                cell: this.getCell(coordStart),
                posStart: this.getPositionFromCoords(coordStart),
                posEnd: this.getPositionFromCoords(coordEnd)
            },
            {
                cell: this.getCell(coordEnd),
                posStart: this.getPositionFromCoords(coordEnd),
                posEnd: this.getPositionFromCoords(coordStart)
            }
        ];
        var _loop_4 = function (value) {
            if (value.cell.status == stateEnums_1.stateCell.active && value.cell.node.getStatus() == stateEnums_1.stateTile.active) {
                value.cell.status = stateEnums_1.stateCell.swipe;
                var timeoutFirst = value.cell.node.setPos(value.posEnd, true);
                this_4.scheduleOnce(function () {
                    var timeoutSecond = value.cell.node.setPos(value.posStart, true);
                    _this.scheduleOnce(function () {
                        value.cell.status = stateEnums_1.stateCell.active;
                    }, timeoutSecond);
                }, timeoutFirst);
            }
        };
        var this_4 = this;
        for (var _i = 0, data_4 = data; _i < data_4.length; _i++) {
            var value = data_4[_i];
            _loop_4(value);
        }
        return true;
    };
    /**
     * Движение с точки на точку
     */
    ArenaController.prototype.tileSwipe = function (coordStart, coordEnd) {
        var data = [
            {
                cell: this.getCell(coordStart),
                coordsNew: coordEnd
            },
            {
                cell: this.getCell(coordEnd),
                coordsNew: coordStart
            }
        ];
        for (var _i = 0, data_5 = data; _i < data_5.length; _i++) {
            var value = data_5[_i];
            if (value.cell.status == stateEnums_1.stateCell.active && value.cell.node.getStatus() == stateEnums_1.stateTile.active) {
                var coordsNew = this.getPositionFromCoords(value.coordsNew);
                value.cell.node.setPos(coordsNew, true);
            }
            this._meta[value.coordsNew.row][value.coordsNew.column] = value.cell;
        }
        var metaCell1 = this._subMeta[coordStart.row][coordStart.column];
        var metaCell2 = this._subMeta[coordEnd.row][coordEnd.column];
        this._subMeta[coordStart.row][coordStart.column] = metaCell2;
        this._subMeta[coordEnd.row][coordEnd.column] = metaCell1;
        return true;
    };
    /**
     * Удаление тайла
     * @param x - Позиция точки
     * @param y - Позиция точки
     */
    ArenaController.prototype.tileRemove = function (coords) {
        var cell = this._meta[coords.row][coords.column];
        if (cell.node.isTile) {
            this.gameAddRequire(cell.node.getType());
            cell.node.runAnimation("destroy");
        }
        if (cell.node.isGift) {
            cell.node.startRemove();
        }
        cell.status = stateEnums_1.stateCell.destroy;
        this._subMeta[coords.row][coords.column] = null;
        this.scheduleOnce(function () {
            if (cell.status != stateEnums_1.stateCell.destroy) {
                return;
            }
            if (cell.node.isValid) {
                cell.node.destroy();
            }
            cell.status = stateEnums_1.stateCell.hole;
            cell.node = null;
        }, config_1.config.tileDestroySpeed);
        return true;
    };
    /*******************************************
     **** Работа с сценой
     *******************************************/
    ArenaController.prototype.getSceneStatus = function () {
        return this._sceneStatus;
    };
    ArenaController.prototype.setSceneStatus = function (status, timeout) {
        var _this = this;
        if (timeout === void 0) { timeout = 0; }
        var oldStatus = this._sceneStatus;
        this._sceneStatus = status;
        if (timeout == 0) {
            return;
        }
        this.scheduleOnce(function () {
            if (_this._sceneStatus != status) {
                return;
            }
            _this._sceneStatus = oldStatus;
        }, timeout);
    };
    /**
     * Поиск дырок, в которые могут упасть тайлы
     */
    ArenaController.prototype.moveHolesVertical = function () {
        var _this = this;
        var sceneStatus = this.getSceneStatus();
        if (sceneStatus == SceneStatus.exploding) {
            return;
        }
        var haveHoles = false;
        var oldColumn = 0;
        var holeCounts = 0;
        this.getEveryCell(function (column, row) {
            if (oldColumn != column) {
                oldColumn = column;
                holeCounts = 0;
            }
            var coordSwipeFrom = new position_1.Coords(column, row);
            var coordSwipeTo = new position_1.Coords(column, row + holeCounts);
            var cell = _this.getCell(coordSwipeFrom);
            if (cell.status == stateEnums_1.stateCell.active && cell.node.getStatus() == stateEnums_1.stateTile.active && holeCounts > 0) {
                _this.tileSwipe(coordSwipeFrom, coordSwipeTo);
                haveHoles = true;
                return;
            }
            if (cell.status == stateEnums_1.stateCell.hole) {
                holeCounts++;
                return;
            }
            holeCounts = 0;
        }, true, true);
        if (haveHoles && sceneStatus == SceneStatus.active) {
            this.setSceneStatus(SceneStatus.isMoving);
        }
        if (!haveHoles && sceneStatus == SceneStatus.isMoving) {
            this.setSceneStatus(SceneStatus.active);
        }
        return haveHoles;
    };
    ArenaController.prototype.moveHolesDiagonal = function () {
        var _this = this;
        if (this.getSceneStatus() == SceneStatus.exploding) {
            return;
        }
        ;
        var haveHoles = false;
        var oldColumn = 0;
        var coordsHole = [];
        var coordsFinded = [];
        this.getEveryCell(function (column, row) {
            if (oldColumn != column) {
                if (coordsHole.length > 0) {
                    _this.diagonalCheck(oldColumn, coordsHole);
                    coordsHole = [];
                }
                oldColumn = column;
            }
            var cell = _this.getCell(new position_1.Coords(column, row));
            if (cell.status == stateEnums_1.stateCell.banned || cell.status == stateEnums_1.stateCell.blocked) {
                if (coordsFinded.length > 0) {
                    coordsHole.push(coordsFinded[coordsFinded.length - 1]);
                }
                coordsFinded = [];
                return;
            }
            if (cell.status != stateEnums_1.stateCell.hole && coordsFinded.length > 0) {
                coordsFinded = [];
                return;
            }
            if (cell.status != stateEnums_1.stateCell.hole) {
                return;
            }
            coordsFinded.push(row);
        }, true, true);
        if (coordsHole.length > 0) {
            this.diagonalCheck(oldColumn, coordsHole);
            coordsHole = [];
        }
        return haveHoles;
    };
    ArenaController.prototype.diagonalCheck = function (column, coordsHole) {
        for (var _i = 0, coordsHole_1 = coordsHole; _i < coordsHole_1.length; _i++) {
            var coordRow = coordsHole_1[_i];
            var coordHole = new position_1.Coords(column, coordRow);
            var coordTopLeft = new position_1.Coords(column - 1, coordRow - 1);
            var coordTopRight = new position_1.Coords(column + 1, coordRow - 1);
            var cellLeft = this.getCell(coordTopLeft);
            var cellRight = this.getCell(coordTopRight);
            if (cellLeft.status == stateEnums_1.stateCell.banned && cellRight.status == stateEnums_1.stateCell.banned) {
                this.getCell(coordHole).status = stateEnums_1.stateCell.blocked;
            }
            if (cellLeft.status != stateEnums_1.stateCell.active && cellRight.status != stateEnums_1.stateCell.active) {
                continue;
            }
            var cellToDrop = 0;
            if (cellLeft.status == stateEnums_1.stateCell.active && cellRight.status == stateEnums_1.stateCell.active) {
                cellToDrop = randomInteger(0, 1);
            }
            else {
                if (cellLeft.status == stateEnums_1.stateCell.active) {
                    cellToDrop = 0;
                }
                else {
                    cellToDrop = 1;
                }
            }
            if (cellToDrop == 1 && cellRight.node.getStatus() == stateEnums_1.stateTile.active) {
                this.tileSwipe(coordTopRight, coordHole);
            }
            if (cellToDrop == 0 && cellLeft.node.getStatus() == stateEnums_1.stateTile.active) {
                this.tileSwipe(coordTopLeft, coordHole);
            }
        }
    };
    /**
     * Поиск возможностей спавна тайла сверху поля
     */
    ArenaController.prototype.spawnTiles = function () {
        var sceneStatus = this.getSceneStatus();
        if (sceneStatus == SceneStatus.exploding) {
            return;
        }
        var isSpawned = false;
        var spawners = this.jsonLevelData.json.field_generation;
        for (var _i = 0, spawners_1 = spawners; _i < spawners_1.length; _i++) {
            var spawn = spawners_1[_i];
            var coords = new position_1.Coords(spawn.column, spawn.row);
            var cell = this.getCell(coords);
            if (cell.status == stateEnums_1.stateCell.hole) {
                this.spawnTile(coords);
                isSpawned = true;
            }
        }
        if (isSpawned && sceneStatus == SceneStatus.active) {
            this.setSceneStatus(SceneStatus.isDropping);
        }
        if (!isSpawned && sceneStatus == SceneStatus.isDropping) {
            this.setSceneStatus(SceneStatus.active);
        }
        return isSpawned;
    };
    ArenaController.prototype.spawnTile = function (coords) {
        var spawnID = coords.column + "_" + coords.row;
        var excludeType = this._spawnerData[spawnID];
        var type = this.tileGetRandomType([excludeType]);
        this._spawnerData[spawnID] = type;
        var node = this.tileCreate(coords, type);
        var pos = this.getPositionFromCoords(coords);
        node.setPos(new cc.Vec2(pos.x, pos.y + config_1.config.cellSize)); // Выставление на клетку выше чем спавн
        node.setPos(pos, true); // Движение клетки вниз до нужной точки
    };
    /**
     * Поиск не упавших нодов
     */
    ArenaController.prototype.checkTiles = function () {
        var _this = this;
        var haveInDropping = false;
        this.getEveryCell(function (column, row) {
            if (haveInDropping) {
                return;
            }
            var cell = _this.getCell(new position_1.Coords(column, row));
            if (cell.status == stateEnums_1.stateCell.destroy) {
                haveInDropping = true;
            }
            if (cell.status != stateEnums_1.stateCell.active) {
                return;
            }
            if (cell.node.getStatus() == stateEnums_1.stateTile.moving) {
                haveInDropping = true;
            }
        });
        if (this.getSceneStatus() == SceneStatus.exploding && !haveInDropping) {
            this.setSceneStatus(SceneStatus.active);
        }
        return haveInDropping;
    };
    /*******************************************
     **** Работа с координатами
     *******************************************/
    /**
     * Получение позиции точки из глобальный координат
     */
    ArenaController.prototype.getCoordFromPosition = function (pos) {
        var column = Math.floor((pos.x - this._arenaPaddingX) / config_1.config.cellSize);
        if (column > config_1.config.fieldSizeX - 1) {
            column = -1;
        }
        var row = Math.floor(Math.abs(config_1.config.canvasSizeY - pos.y - this._arenaPaddingY) / config_1.config.cellSize);
        if (row > config_1.config.fieldSizeY - 1) {
            row = -1;
        }
        return new position_1.Coords(column, row);
    };
    /**
     * Получение глобальной позиции из локальных координат точки
     */
    ArenaController.prototype.getPositionFromCoords = function (coords) {
        var column = this._arenaPaddingX + config_1.config.cellSize * (coords.column) + config_1.config.cellSize / 2;
        var row = Math.abs(config_1.config.canvasSizeY - this._arenaPaddingY - config_1.config.cellSize * (coords.row) - config_1.config.cellSize / 2);
        return new cc.Vec2(column, row);
    };
    /*******************************************
     **** Добавление ходов и целей
     *******************************************/
    ArenaController.prototype.gameAddRequire = function (type) {
        if (this._requireType == type) {
            this._requirePickedNow += 1;
        }
    };
    ArenaController.prototype.gameAddMoves = function () {
        if (!this._boosterIsActive) {
            this._movesNow += 1;
            return;
        }
        this._boosterIsActive = false;
        this._boosterIsUsed = true;
        this.buttonBooster.getComponent(cc.Animation).stop("boosterClick");
    };
    /*******************************************
     **** Отображение информации
     *******************************************/
    ArenaController.prototype.drawInfo = function () {
        var nowMoves = this._moves - this._movesNow;
        var nowRequire = this._requirePicked - this._requirePickedNow;
        this.labelMoves.string = nowMoves < 0 && "0" || nowMoves + "";
        this.labelRequire.string = nowRequire < 0 && "0" || nowRequire + "";
        var spriteFrameRequire = this[stateEnums_1.stateTileSprite[this._requireType]];
        if (this.spriteRequire.spriteFrame != spriteFrameRequire) {
            this.spriteRequire.spriteFrame = spriteFrameRequire;
        }
    };
    /*******************************************
     **** Проверка целей
     *******************************************/
    ArenaController.prototype.checkRequires = function () {
        if (this.getSceneStatus() != SceneStatus.active) {
            return;
        }
        var nowMoves = (this._moves - this._movesNow);
        var nowRequire = (this._requirePicked - this._requirePickedNow);
        if (nowMoves <= 0 && nowRequire > 0) {
            cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName_1.EventName.MenuFail, true));
            this.setSceneStatus(SceneStatus.menu);
            return;
        }
        if (nowRequire <= 0) {
            this.gameClear();
            cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName_1.EventName.MenuWinner, true));
            this.setSceneStatus(SceneStatus.menu);
            return;
        }
    };
    /*******************************************
     **** Кнопки на поле
     *******************************************/
    /**
     * Кнопка бустера
     */
    ArenaController.prototype.fButtonBooser = function (event) {
        if (this._boosterIsUsed || this._boosterIsActive) {
            return;
        }
        this._boosterIsActive = true;
        this.buttonBooster.getComponent(cc.Animation).play("boosterClick");
    };
    ArenaController.prototype.start = function () {
        // Вычесление отступов на поле
        this._arenaPaddingX = (config_1.config.canvasSizeX - (config_1.config.cellSize * config_1.config.fieldSizeX)) / 2;
        this._arenaPaddingY = (config_1.config.canvasSizeY - (config_1.config.cellSize * config_1.config.fieldSizeY)) / 2;
    };
    /*******************************************
     **** Update func per frame
     *******************************************/
    ArenaController.prototype.update = function (dt) {
        var sceneState = this.getSceneStatus();
        if (sceneState == SceneStatus.menu || sceneState == SceneStatus.spawning) {
            return;
        }
        this.moveHolesVertical();
        this.moveHolesDiagonal();
        this.spawnTiles();
        var inMovingTiles = this.checkTiles();
        if (inMovingTiles == false) {
            this.metaFindAllCombinations();
        }
        this.drawInfo();
        this.checkRequires();
        if (this._touchTileNode != null && this._touchTileNode.isValid) {
            this.spriteChooser.node.active = true;
            this.spriteChooser.node.setPosition(this._touchTileNode.node.getPosition());
        }
        else {
            if (this.spriteChooser.node.active != false) {
                this.spriteChooser.node.active = false;
            }
        }
    };
    __decorate([
        property(cc.Sprite)
    ], ArenaController.prototype, "spriteChooser", void 0);
    __decorate([
        property(cc.Sprite)
    ], ArenaController.prototype, "spriteRequire", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFrameRed", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFrameOrange", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFrameYellow", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFrameGreen", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFrameBlue", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], ArenaController.prototype, "spriteFramePurple", void 0);
    __decorate([
        property(cc.Label)
    ], ArenaController.prototype, "labelMoves", void 0);
    __decorate([
        property(cc.Label)
    ], ArenaController.prototype, "labelRequire", void 0);
    __decorate([
        property(cc.Node)
    ], ArenaController.prototype, "buttonBooster", void 0);
    __decorate([
        property(cc.JsonAsset)
    ], ArenaController.prototype, "jsonLevelData", void 0);
    __decorate([
        property(cc.Prefab)
    ], ArenaController.prototype, "itemGiftPrefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], ArenaController.prototype, "itemTilePrefab", void 0);
    __decorate([
        property(cc.Prefab)
    ], ArenaController.prototype, "cellItemHole", void 0);
    __decorate([
        property(cc.Prefab)
    ], ArenaController.prototype, "cellItemBlock", void 0);
    __decorate([
        property(cc.Node)
    ], ArenaController.prototype, "arenaTiles", void 0);
    __decorate([
        property(cc.Node)
    ], ArenaController.prototype, "arenaHoles", void 0);
    __decorate([
        property(cc.Node)
    ], ArenaController.prototype, "arenaBlocks", void 0);
    ArenaController = __decorate([
        ccclass
    ], ArenaController);
    return ArenaController;
}(cc.Component));
exports.ArenaController = ArenaController;
/**
 * Рандомное целочисленное значение
 * @param min - от включительно
 * @param max - до включительно
 */
var randomInteger = function (min, max) {
    var rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
};

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Main.js.map
        