(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/functions/position.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'f4a14Sd0uxJdJ7I1dyaXAYU', 'position', __filename);
// scripts/arena/functions/position.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Coords = /** @class */ (function () {
    function Coords(column, row) {
        this.column = 0; // x
        this.row = 0; // y
        this.column = column;
        this.row = row;
    }
    return Coords;
}());
exports.Coords = Coords;
exports.CoordAxis = {
    row: "row",
    column: "column"
};

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=position.js.map
        