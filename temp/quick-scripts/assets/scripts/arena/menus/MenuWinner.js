(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/menus/MenuWinner.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '9895enm/X1DL7Xy89BRsBQJ', 'MenuWinner', __filename);
// scripts/arena/menus/MenuWinner.ts

"use strict";
/**
 * Imports modules
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventName_1 = require("../EventName");
/**
 * Class Constructor
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MenuWinner = /** @class */ (function (_super) {
    __extends(MenuWinner, _super);
    function MenuWinner() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**********************************
         **** @property
         **********************************/
        // Кнопки
        _this.buttonNewGame = null; // Новая игра
        _this.buttonExit = null; // Кнопка выхода
        return _this;
    }
    /**********************************
     **** @functions
     **********************************/
    MenuWinner.prototype.onLoad = function () {
        // Global
        cc.systemEvent.on(EventName_1.EventName.MenuWinner, this.show, this);
        // Buttons
        this.buttonNewGame.on(cc.Node.EventType.TOUCH_START, this.fButtonNewGame, this);
        this.buttonExit.on(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
        // Показ нода главного меню
        this.node.active = false;
    };
    MenuWinner.prototype.onDestroy = function () {
        // Global
        cc.systemEvent.off(EventName_1.EventName.MenuWinner, this.show, this);
        // Buttons
        this.buttonNewGame.off(cc.Node.EventType.TOUCH_START, this.fButtonNewGame, this);
        this.buttonExit.off(cc.Node.EventType.TOUCH_START, this.fButtonExit, this);
    };
    // Закрытие и открытие окна
    MenuWinner.prototype.show = function () {
        this.node.active = true;
        this.getComponent(cc.Animation).play("menuFadeIN");
    };
    MenuWinner.prototype.onClose = function () {
        this.getComponent(cc.Animation).off("finished", this.onClose, this);
        this.node.active = false;
    };
    MenuWinner.prototype.close = function () {
        this.getComponent(cc.Animation).play("menuFadeOUT");
        this.getComponent(cc.Animation).on("finished", this.onClose, this);
    };
    // Функции кнопок
    MenuWinner.prototype.fButtonNewGame = function (event) {
        this.close();
        cc.systemEvent.dispatchEvent(new cc.Event.EventCustom(EventName_1.EventName.GameStart, true));
    };
    MenuWinner.prototype.fButtonExit = function (event) {
        cc.game.end();
    };
    MenuWinner.prototype.update = function (dt) { };
    __decorate([
        property(cc.Node)
    ], MenuWinner.prototype, "buttonNewGame", void 0);
    __decorate([
        property(cc.Node)
    ], MenuWinner.prototype, "buttonExit", void 0);
    MenuWinner = __decorate([
        ccclass
    ], MenuWinner);
    return MenuWinner;
}(cc.Component));
exports.default = MenuWinner;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=MenuWinner.js.map
        