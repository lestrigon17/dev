(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/arena/stateEnums.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '2a197KOy8FJN7QhLGZ/fWPR', 'stateEnums', __filename);
// scripts/arena/stateEnums.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
/**
 * Tile types
 */
var stateTileType;
(function (stateTileType) {
    stateTileType[stateTileType["red"] = 0] = "red";
    stateTileType[stateTileType["orange"] = 1] = "orange";
    stateTileType[stateTileType["yellow"] = 2] = "yellow";
    stateTileType[stateTileType["green"] = 3] = "green";
    stateTileType[stateTileType["blue"] = 4] = "blue";
    stateTileType[stateTileType["purple"] = 5] = "purple";
})(stateTileType = exports.stateTileType || (exports.stateTileType = {}));
function getRandomTileType() {
    var types = [];
    var typesCount = Object.keys(stateTileType).length / 2;
    for (var i = 0; i < typesCount; i++) {
        types.push(stateTileType[stateTileType[i]]);
    }
    return types;
}
exports.getRandomTileType = getRandomTileType;
/**
 * Tile sprite of type
 */
exports.stateTileSprite = (_a = {},
    _a[stateTileType.red] = "spriteFrameRed",
    _a[stateTileType.orange] = "spriteFrameOrange",
    _a[stateTileType.yellow] = "spriteFrameYellow",
    _a[stateTileType.green] = "spriteFrameGreen",
    _a[stateTileType.blue] = "spriteFrameBlue",
    _a[stateTileType.purple] = "spriteFramePurple",
    _a);
/**
 * Tile status
 */
var stateTile;
(function (stateTile) {
    stateTile[stateTile["spawning"] = 0] = "spawning";
    stateTile[stateTile["active"] = 1] = "active";
    stateTile[stateTile["moving"] = 2] = "moving";
    stateTile[stateTile["destroy"] = 3] = "destroy";
    stateTile[stateTile["busy"] = 4] = "busy";
})(stateTile = exports.stateTile || (exports.stateTile = {}));
/**
 * Cell status
 */
var stateCell;
(function (stateCell) {
    stateCell[stateCell["banned"] = 0] = "banned";
    stateCell[stateCell["blocked"] = 1] = "blocked";
    stateCell[stateCell["active"] = 2] = "active";
    stateCell[stateCell["hole"] = 3] = "hole";
    stateCell[stateCell["destroy"] = 4] = "destroy";
    stateCell[stateCell["swipe"] = 5] = "swipe";
})(stateCell = exports.stateCell || (exports.stateCell = {}));
/**
 * Gift  type
 */
var stateGiftType;
(function (stateGiftType) {
    stateGiftType[stateGiftType["horizontal"] = 100] = "horizontal";
    stateGiftType[stateGiftType["vertical"] = 101] = "vertical";
})(stateGiftType = exports.stateGiftType || (exports.stateGiftType = {}));

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=stateEnums.js.map
        